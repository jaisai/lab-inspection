<%@page import="labInspection.DataCRUD"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<html>
    <head>
        <title>Lab Inspection Audit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="css/jquery.tablesorter_1.js" type="text/javascript"></script>
        <script src="css/jquery.tablesorter.widgets.js" type="text/javascript"></script>
        <script src="css/widget-scroller.js" type="text/javascript"></script>
    </head>
    <script>
        function goBack() {
            window.history.back();
        }

        $(document).ready(function ()
        {
            $('#UserView').tablesorter({
                cssHeader: ".tablesorter-header",
                cssNone: ".tablesorter-headerUnSorted",
                cssAsc: ".tablesorter-headerAsc",
                cssDesc: ".tablesorter-headerDesc",
                theme: 'jui',
                showProcessing: true,
                headerTemplate: '{content} {icon}',
                widgets: ['uitheme', 'zebra', 'scroller'],
                widgetOptions: {
                    scroller_height: 500,
                    // scroll tbody to top after sorting
                    scroller_upAfterSort: true,
                    // pop table header into view while scrolling up the page
                    scroller_jumpToHeader: true,
                    // In tablesorter v2.19.0 the scroll bar width is auto-detected
                    // add a value here to override the auto-detected setting
                    scroller_barWidth: null
                            // scroll_idPrefix was removed in v2.18.0
                            // scroller_idPrefix : 's_'
                }


            });

            $('#responses').tablesorter({
                cssHeader: ".tablesorter-header",
                cssNone: ".tablesorter-headerUnSorted",
                cssAsc: ".tablesorter-headerAsc",
                cssDesc: ".tablesorter-headerDesc",
                theme: 'jui',
                sortList: [[1, 0]],
                showProcessing: true,
                headerTemplate: '{content} {icon}',
                widgets: ['uitheme', 'zebra', 'scroller'],
                widgetOptions: {
                    scroller_height: 500,
                    // scroll tbody to top after sorting
                    scroller_upAfterSort: true,
                    // pop table header into view while scrolling up the page
                    scroller_jumpToHeader: true,
                    // In tablesorter v2.19.0 the scroll bar width is auto-detected
                    // add a value here to override the auto-detected setting
                    scroller_barWidth: null
                            // scroll_idPrefix was removed in v2.18.0
                            // scroller_idPrefix : 's_'
                }


            });



        }
        );

        $(document).ready(function () {
            $('#sendToInspector').click(function () {
                filled = $("input[type=facComment]");

                var empty = 0;

                for (var i = 0; i < filled.length; i++) {
                    if (filled[i].value === "") {

                        empty = 1;
                    }
                }

                if (empty === 0) {
                    document.getElementById("FCA").submit();
                } else {
                    alert("You must fill each field");
                }
            });
        });

    </script>
    <body class="userView">

        <nav class="navbar navbar-default">
            <div class="test-center">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                <% String fullName = (String) session.getAttribute("fullName");
                    if (fullName == null) {
                        fullName = (String) request.getAttribute("fullName");
                    }
                %>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <button type="button" class="btn btn-default navbar-btn pull-right" onclick="window.location.href = 'Login.jsp'" >
                    <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
            </div>
        </nav>
        <h2 align="center">&nbsp; &nbsp; &nbsp; &nbsp; Lab Inspection Audit</h2>
        <div class="container"  style="margin:auto; border:2px solid #258e12; border-radius:15px; width:68%; background: #ffffff"> 

            <form id="FCA"  action="FacultyCorrectiveActions" method="post">

                <%
                    String ResponsibleIndividualName = (String) request.getAttribute("ResponsibleIndividualName");
                    String RoomAssignment = (String) request.getAttribute("RoomAssignment");
                    String roomNumber = (String) request.getAttribute("RoomNumber");
                    String formattedInspectionDate = (String) request.getAttribute("InspectionDate");
                    int labId = (Integer) request.getAttribute("correctiveActionsLabId");
                    request.getSession().setAttribute("userViewTable", DataCRUD.userViewTable(labId));
                    String userName = (String) session.getAttribute("userName");
                    int completed = (Integer) request.getAttribute("Completed");
                    String viewSwitch = (String) session.getAttribute("viewSwitch");


                %>
                <input hidden="hidden" name="labId" value="<%=labId%>">
                <input hidden="hidden" name="fullName" value="<%=fullName%>">
                <input hidden="hidden" name="userName" value="<%=userName%>">
                <input hidden="hidden" name="Completed" value="<%=completed%>">
                <input hidden="hidden" name="viewSwitch" value="<%=viewSwitch%>">
                
                    
                        <div class="form"  style="margin-top:2%" >
                        <table id="UserView" class="tablesorter" style="overflow-x: scroll">
                            <thead>
                                <tr>
                                    <th>Responsible Individual:</th>
                                    <th>Room Assignment:</th>
                                    <th>Room Number:</th>
                                    <th>Inspection Date:</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><%=ResponsibleIndividualName%></td>
                                    <td><%=RoomAssignment%></td>
                                    <td><%=roomNumber%></td>
                                    <td><%=formattedInspectionDate%></td>
                                </tr>
                            </tbody>
                        </table>

                        <br>

                        <table id="responses" class="tablesorter" style="overflow-x: auto">
                            <thead>
                            <th>Question</th>
                            <th>Compliance</th>
                            <th>Original Comments</th>
                            <th>Faculty Comments</th>
                            <th>Reviewer Comments</th>
                            </thead>

                            <tbody>

                                <c:forEach  items="${userViewTable}" var="lab">
                                    <tr>
                                        <td id="question">${lab.question}</td>
                                        <td id="compliance">${lab.compliant}</td>
                                        <td id="originalComments">${lab.originalComments}</td>
                                        <td>
                                            <c:if test="${lab.compliant == 'Yes'}">
                                                <span>${lab.facultyComments}</span>
                                                <input hidden="hidden"  name="facultyComments" value="${lab.facultyComments}">
                                            </c:if>
                                            <c:if test="${lab.compliant == 'No'}">
                                                <span>${lab.facultyComments}</span>
                                                <br/>
                                                <input type='facComment' name="facultyCommentsNew" type="text">
                                                <input hidden="hidden"  name="facultyComments" value="${lab.facultyComments}">
                                            </c:if>
                                        </td>

                                        <td id="adminComments">${lab.adminComments}</td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>

                        <%if (completed == 1) {%>

                        <h1><span class="label label-success">This lab is compliant, please select Send to finish the lab</span></h1>

                        <%}%>

                        <button style="width:10%; margin-right: 30%; margin-bottom: 2%; text-align: center;" type="button" id='sendToInspector' name="sendToInspector" class="btn btn-primary pull-right">Send</button>
                        <button style="width:10%; margin-left: 30%; margin-bottom: 2%; text-align: center;" type="submit" name="save" class="btn btn-primary pull-left">Save</button>
                    </div>
                
            </form>
        </div>

    </body>
</html>

