<%-- 
    Document   : LabDetails
    Created on : Oct 12, 2016, 3:23:54 PM
    Author     : S525096
--%>


<%@page import="labInspection.DataCRUD"%>
<%@page import="labInspection.LabAudit"%>
<%@page import="java.util.List"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lab Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            function assign(id)
            {
                var edittextbox = document.getElementById("EditLabId");
                edittextbox.value = "" + id;
                document.EditLabForm.action = "EditLab";
                document.EditLabForm.submit();

            }


        </script>
    </head>
    <script>
        function goBack() {
            window.history.back();
        }


    </script>
    <body>

        <nav class="navbar navbar-default">
            <div class="test-center">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 
                    </a>
                </ul>
                <% String fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <button type="button" class="btn btn-default navbar-btn pull-right" onclick="window.location.href = 'Login.jsp'" >
                    <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
            </div>
        </nav>
        <div style="width: 1200px; margin-left: auto; margin-right: auto;">
            <%
                String exportToExcel = request.getParameter("exportToExcel");
                if (exportToExcel != null && exportToExcel.toString().equalsIgnoreCase("YES")) {
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "inline; filename=" + "excel.xls");
                }
                request.getSession().setAttribute("inspectionLabs", DataCRUD.getLabDetails());


            %>
            <form name="EditLabForm" method="post" action="">

                <div><br /></div>
                <div class="container">
                    <table border='1' class="table" table-hover>
                        <thead>
                            <tr>
                                <th>Lab Id</th>
                                <th>Building</th>
                                <th>Room Number</th>
                                <th>Discipline</th>
                                <th>Responsible Individual</th>
                                <th>Room Assignment</th>
                                <th>Inspection Date</th>
                                <th>Status</th>
                            </tr>
                        </thead> 
                        <c:forEach items="${inspectionLabs}" var="lab">
                            <tbody>
                                <tr>
                                    <td>${lab.labId}</td>
                                    <td>${lab.building}</td>
                                    <td>${lab.roomNumber}</td>
                                    <td>${lab.discipline}</td>
                                    <td>${lab.responsibleInduvidualName}</td>
                                    <td>${lab.roomAssignment}</td>
                                    <td>${lab.inspectionDate}</td>
                                    <td>${lab.status}</td>
                                    <td><input type="Submit" onclick="assign(${lab.labId})" name="${lab.labId}edit" id="${lab.labId}edit" value="Edit" style="background-color:#49743D;font-weight:bold;color:#ffffff;"></td>

                                </tr>
                            </tbody>
                        </c:forEach>
                    </table>
                    <input type="hidden" name="EditLabId" value="" id="EditLabId"/>
                </div>

                <%                    if (exportToExcel == null) {
                %>
                <a href="LabDetails.jsp?exportToExcel=YES">Export to Excel</a>
                <%
                    }
                %>
                <br>
                <!--                Room Number <input type="text"/>
                                <input type="submit" value="Filter"/>-->
            </form>
        </div>
    </body>
</html>
