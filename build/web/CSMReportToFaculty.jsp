<%-- 
    Document   : CSMReportToFaculty
    Created on : Nov 30, 2016, 3:59:35 PM
    Author     : S525051
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lab Inspection Audit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>   
    </head>
    <script>
        function goBack() {
            window.history.back();
        }


    </script>
    <body>
        <nav class="navbar navbar-default">
            <div class="test-center">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>
                    
                        <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 
                    
                </ul>
                <% String fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <button type="button" class="btn btn-default navbar-btn pull-right" onclick="window.location.href = 'Login.jsp'" >
                    <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
            </div>
        </nav>
        <form name="CSMReportForm" action="">
            <div style="width: 1200px; margin-left: auto; margin-right: auto;">
                <div class="container">
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Question</th>
                                <th>Response</th>
                                <th>Comments</th>

                            </tr>

                        </thead>

                        <tbody>
                            <c:forEach items="${CSMResponses}" var="csmresponse">
                                <c:choose>
                                    <c:when test="${csmresponse.questionId==1}">
                                        <tr> 
                                            <td><b>General Safety</b></td>
                                        <tr>
                                        </c:when>
                                        <c:when test="${csmresponse.questionId==7}">
                                        <tr>
                                            <td><b> Signs </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==16}">
                                        <tr>
                                            <td> <b> Fire Protection </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==19}">
                                        <tr>
                                            <td> <b> Chemical Storage </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==31}">
                                        <tr>
                                            <td> <b> Waste Disposal </b></td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==40}">
                                        <tr>
                                            <td> <b> Safety Equipment </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==49}">
                                        <tr>
                                            <td> <b> Biological Safety </b></td>
                                        </tr>

                                    </c:when>
                                    <c:when test="${csmresponse.questionId==52}">
                                        <tr>
                                            <td> <b> Biological Contamination Control </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==57}">
                                        <tr>
                                            <td> <b> Biohazard Waste Handling </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==61}">
                                        <tr>
                                            <td> <b> Equipment Management </b> </td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==65}">
                                        <tr>
                                            <td> <b> Fume Foods </b></td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${csmresponse.questionId==70}">
                                        <tr>
                                            <td> <b> Compressed Gas Cylinders </b> </td>
                                        </tr>
                                    </c:when>
                                </c:choose>
                                <tr>
                                    <td>
                                        ${csmresponse.question}
                                    </td>
                                    <td>
                                        ${csmresponse.compliant}
                                    </td>
                                    <td>
                                        ${csmresponse.comments}       
                                    </td>
                                </tr>

                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
