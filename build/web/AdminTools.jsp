<%-- 
    Document   : AdminTools
    Created on : Feb 15, 2017, 4:09:54 PM
    Author     : s528180
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@page import="labInspection.DataCRUD"%>
<%@page import="labInspection.LabAudit"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab Inspection Audit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="css/jquery-latest.js" type="text/javascript"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="css/jquery.tablesorter_1.js" type="text/javascript"></script>
        <script src="css/jquery.tablesorter.widgets.js" type="text/javascript"></script>
        <script src="css/widget-scroller.js" type="text/javascript"></script>
        <script>
            function goBack() {
                window.history.back();
            }

            //calling table sorter function
            $(document).ready(function ()
            {

                $.tablesorter.addParser({
                    // add parser through the tablesorter addParser method$.tablesorter.addParser({
                    // set a unique id
                    id: 'Building',
                    is: function (s) {
                        // return false so this parser is not auto detected
                        return false;
                    },
                    format: function (s, table, cell) {
                        // format your data for normalization

                        var inner = cell.innerHTML;
                        var start = inner.indexOf("value=") + 7;
                        var end = inner.indexOf(">", start) - 1;
                        var text = inner.substring(start, end);
                        var textToReturn = text.replace(/-/g, ' ');
                        return textToReturn;
                    },
                    // set type, either numeric or text
                    type: 'text'
                });

                $.tablesorter.addParser({
                    // add parser through the tablesorter addParser method$.tablesorter.addParser({
                    // set a unique id
                    id: 'ResponsibleIndividual',
                    is: function (s) {
                        // return false so this parser is not auto detected
                        return false;
                    },
                    format: function (s, table, cell) {
                        // format your data for normalization
                        var inner = cell.innerHTML;
                        var start = inner.indexOf("value=") + 7;
                        var end = inner.indexOf(">", start) - 1;
                        var text = inner.substring(start, end);
                        var textToReturn = text.replace(/,/g, ' ');
                        return textToReturn;
                    },
                    // set type, either numeric or text
                    type: 'text'
                });

                $.tablesorter.addParser({
                    // add parser through the tablesorter addParser method$.tablesorter.addParser({
                    // set a unique id
                    id: 'RoomAssignment',
                    is: function (s) {
                        // return false so this parser is not auto detected
                        return false;
                    },
                    format: function (s, table, cell) {
                        // format your data for normalization

                        var inner = cell.innerHTML;
                        var start = inner.indexOf("value=") + 7;
                        var end = inner.indexOf(">", start) - 1;
                        var text = inner.substring(start, end);
                        var textToReturn = text.replace(/\//, ' ');
                        return textToReturn;
                    },
                    // set type, either numeric or text
                    type: 'text'
                });


                $('#addQuestion').tablesorter({
                    theme: 'jui',
                    showProcessing: true,
                    headerTemplate: '{content} {icon}',
                    widgets: ['uitheme', 'zebra', 'scroller'],
                    widgetOptions: {
                        scroller_height: 300,
                        // scroll tbody to top after sorting
                        scroller_upAfterSort: true,
                        // pop table header into view while scrolling up the page
                        scroller_jumpToHeader: true,
                        // In tablesorter v2.19.0 the scroll bar width is auto-detected
                        // add a value here to override the auto-detected setting
                        scroller_barWidth: null,
                    }
                });
                $('#addLab').tablesorter({
                    theme: 'jui',
                    showProcessing: true,
                    headerTemplate: '{content} {icon}',
                    widgets: ['uitheme', 'zebra', 'scroller'],
                    widgetOptions: {
                        scroller_height: 300,
                        // scroll tbody to top after sorting
                        scroller_upAfterSort: true,
                        // pop table header into view while scrolling up the page
                        scroller_jumpToHeader: true,
                        // In tablesorter v2.19.0 the scroll bar width is auto-detected
                        // add a value here to override the auto-detected setting
                        scroller_barWidth: null,
                    }
                });
                $('#lab_table').tablesorter({
                    headers: {
                        '.buttons, .roomNumber': {
                            sorter: false
                        },
                        0: {
                            sorter: 'Building'
                        },
                        1: {
                            sorter: false
                        },
                        2: {
                            sorter: 'Building' //this sorter doesn't really matter, it just needs to be extracted from <input> tag
                        },
                        3: {
                            sorter: 'ResponsibleIndividual'
                        },
                        4: {
                            sorter: 'RoomAssignment'
                        },
                        5: {
                            sorter: false
                        }
                    },
                    cssHeader: ".tablesorter-header",
                    cssNone: ".tablesorter-headerUnSorted",
                    cssAsc: ".tablesorter-headerAsc",
                    cssDesc: ".tablesorter-headerDesc",
                    theme: 'jui',
                    showProcessing: true,
                    headerTemplate: '{content} {icon}',
                    widgets: ['uitheme', 'zebra', 'scroller'],
                    widgetOptions: {
                        scroller_height: 300,
                        // scroll tbody to top after sorting
                        scroller_upAfterSort: true,
                        // pop table header into view while scrolling up the page
                        scroller_jumpToHeader: true,
                        // In tablesorter v2.19.0 the scroll bar width is auto-detected
                        // add a value here to override the auto-detected setting
                        scroller_barWidth: null,
                    }


                });
                $('#question_table').tablesorter({
                    cssHeader: ".tablesorter-header",
                    cssNone: ".tablesorter-headerUnSorted",
                    cssAsc: ".tablesorter-headerAsc",
                    cssDesc: ".tablesorter-headerDesc",
                    theme: 'jui',
                    showProcessing: true,
                    headerTemplate: '{content} {icon}',
                    widgets: ['uitheme', 'zebra', 'scroller'],
                    widgetOptions: {
                        scroller_height: 300,
                        // scroll tbody to top after sorting
                        scroller_upAfterSort: true,
                        // pop table header into view while scrolling up the page
                        scroller_jumpToHeader: true,
                        // In tablesorter v2.19.0 the scroll bar width is auto-detected
                        // add a value here to override the auto-detected setting
                        scroller_barWidth: null,
                        // scroll_idPrefix was removed in v2.18.0
                        // scroller_idPrefix : 's_'
                        // scroll_idPrefix was removed in v2.18.0
                        // scroller_idPrefix : 's_'

                    }


                });
            }
            );
            window.onload = function () {
                getCategories();
            };

            function getCategories() {
                requestCat = new XMLHttpRequest();
                //creates url to call LabAuditCategories
                var url = "LabAuditCategories";
                requestCat.open("POST", url, true);
                //when onreadystate changes call displayCheckBoxes()
                requestCat.onreadystatechange = displayCategories;
                requestCat.send(null);
            }

            function displayCategories() {

                //readyState 4 = request has finished and ready 
                //staus 200 = OK (a status of 404 = page not found)
                if (requestCat.readyState === 4 && requestCat.status === 200) {
                    // The split produces one extra empty string after the last newline.
                    //gets data from request and puts it into categoryList
                    var categoryList = requestCat.responseText.split("\n");

                    //gets the checkBoxes tag
                    var select = document.getElementById("catSelector");

                    var value = 101;

                    //adds all the options retrieved in disciplineList to discipline field
                    for (var i = 0; i < categoryList.length - 1; ++i) {
                        var option = document.createElement("OPTION");

                        option.id = categoryList[i];
                        option.text = categoryList[i];

                        select.appendChild(option);


                    }
                }

            }

            //the update row variabel goes as follows
            //update lab = 1 update question = 2 add lab = 3 add question = 4 delete lab = 5 delete question = 6

            function updateRowQuestion(questionId) {

                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=2&questionId=" + questionId
                        + "&question=" + document.getElementById("question" + questionId).value
                        + "&categoryId=" + document.getElementById("category" + questionId).value;

                request.open("POST", url, true);
                request.send();

            }

            function deleteRowQuestion(questionId) {

                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=6&questionId=" + questionId;

                request.open("POST", url, true);
                request.send();
                var row = document.getElementById(questionId + "row");
                row.hidden = true;

                $('#question_table')
                        .trigger('update');

            }

            function updateRowLab(labId) {

                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=1&labInfoId=" + labId
                        + "&building=" + document.getElementById("building" + labId).value
                        + "&roomNumber=" + document.getElementById("roomNumber" + labId).value
                        + "&discipline=" + document.getElementById("discipline" + labId).value
                        + "&responsibleIndividual=" + document.getElementById("responsibleIndividual" + labId).value
                        + "&roomAssignment=" + document.getElementById("roomAssignment" + labId).value;

                request.open("POST", url, true);
                request.send();


            }

            function deleteRowLab(labId) {

                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=5&labInfoId=" + labId;

                request.open("POST", url, true);
                request.send();
                var row = document.getElementById(labId + "row");
                row.hidden = true;

            }

            function addRowLab() {
                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=3&building=" + document.getElementById("buildingName").value
                        + "&roomNumber=" + document.getElementById("roomNumberNew").value
                        + "&discipline=" + document.getElementById("disciplineName").value
                        + "&responsibleIndividual=" + document.getElementById("responsiblePerson").value
                        + "&roomAssignment=" + document.getElementById("roomAssignedNew").value;

                request.open("POST", url, true);
                request.onreadystatechange = displayNewLab;
                request.send();

            }

            function displayNewLab() {
                if (request.readyState === 4 && request.status === 200) {

                    var labId = request.responseText.split('\n');

                    var row = '<tr id="' + labId + 'row"><td><input id="building' + labId + '" value="' + document.getElementById("buildingName").value + '">'
                            + '</td><td><input id="roomNumber' + labId + '" value="' + document.getElementById("roomNumberNew").value + '">'
                            + '</td><td><input id="discipline' + labId + '" value="' + document.getElementById("disciplineName").value + '">'
                            + '</td><td><input id="responsibleIndividual' + labId + '" value="' + document.getElementById("responsiblePerson").value + '">'
                            + '</td><td><input id="roomAssignment' + labId + '" value="' + document.getElementById("roomAssignedNew").value + '">'
                            + '</td><td><button type="button" class="btn btn-primary" onclick="updateRowLab(' + labId + ')">Update</button>'
                            + '<button type="button" class="btn btn-primary" onclick="deleteRowLab(' + labId + ')">Delete</button></td></tr>';

                    $('#lab_table')
                            .trigger('addRows', row)
                            .trigger('update');

                    document.getElementById("buildingName").value = "";
                    document.getElementById("buildingName").placeholder = "Building";
                    document.getElementById("roomNumberNew").value = "";
                    document.getElementById("roomNumberNew").placeholder = "Room Number";
                    document.getElementById("disciplineName").value = "";
                    document.getElementById("disciplineName").placeholder = "Discipline";
                    document.getElementById("responsiblePerson").value = "";
                    document.getElementById("responsiblePerson").placeholder = "Responsible Person";
                    document.getElementById("roomAssignedNew").value = "";
                    document.getElementById("roomAssignedNew").placeholder = "Room Assignment";
                }
            }

            function displayNewQuestion() {
                if (request.readyState === 4 && request.status === 200) {

                    var questionId = request.responseText.split('\n');
                    var select = document.getElementById("catSelector");

                    var row = '<tr id="' + questionId + 'row">\n<td hidden="hidden">' + questionId + '</td>\n'
                            + '<td>\n<input type="text" name="categoryType" id="category' + questionId + '" value="' + document.getElementById("catInput").value + '"></td>\n'
                            + '<td>\n<input type="text" name="question" id="question' + questionId + '" value="' + document.getElementById("newQuestion").value + '"></td>\n'
                            + '<td>\n<button type="button" class="btn btn-primary" onclick="updateRowQuestion(' + questionId + ')">Update</button>\n'
                            + '<button type="button" class="btn btn-primary" onclick="deleteRowQuestion(' + questionId + ')">Delete</button>\n</td>\n</tr>';

                    $('#question_table')
                            .find('tbody')
                            .append(row)
                            .trigger('update');

                    var option = document.createElement("OPTION");

                    option.id = document.getElementById("catInput").value;
                    option.text = document.getElementById("catInput").value;

                    select.appendChild(option);

                    document.getElementById("newQuestion").placeholder = "Question";
                    document.getElementById("newQuestion").text = "";
                    document.getElementById("newQuestion").value = "";

                }
            }

            function addRowQuestion() {

                request = new XMLHttpRequest();
                var input = document.getElementById("catInput").value;

                var url = "UpdateLabInformation?updateRow=4" + "&question=" + document.getElementById("newQuestion").value
                        + "&categoryName=" + input;

                request.open("POST", url, true);
                request.onreadystatechange = displayNewQuestion;
                request.send();



            }

            function deleteCategory() {

                var list = document.getElementById("catSelector");
                var input = $("#catInput");
                var children = list.childNodes;
                var category = input.val();
                for (var i = 0; i < children.length; i++) {
                    var currentCat = children[i].label;
                    if (currentCat === category) {
                        input[0].value = "";
                        input[0].innerHTML = "";
                        list.removeChild(children[i]);

                        break;
                    }
                    var nothing = 'hello';
                }

                request = new XMLHttpRequest();

                var url = "UpdateLabInformation?updateRow=7" + "&categoryName=" + category;

                request.open("POST", url, true);
                request.send();




            }
        </script>



    </head>
    <body>
        <nav class="navbar navbar-default" style="background-color: #ffffff; border: #258e12">
            <div class="container-fluid">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                <%String fullName = (String) request.getAttribute("fullName");%>
                <% if (fullName != "" && fullName != null) { %>
                <span class ="navbar-text">Signed in as <% out.print(fullName); %>
                    <%session.setAttribute("fullName", fullName);%></span>
                    <%} else { %>
                    <%fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <% out.print((String) fullName); %>
                    <%session.setAttribute("fullName", fullName);%></span><% } %>


                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
        </nav>

        <form action="UpdateLabInformation" method="post">

            <div style=" margin-left: auto; margin-right: auto;">

                <%
                    request.getSession().setAttribute("Labs", DataCRUD.getLabInformation());

                %>

                <div class="form" style=" margin-left: auto; margin-right: auto;">

                    <div align="center" class="container"  style="border:10px solid #ffffff; border-radius: 15px; background: #ffffff">
                        
                        <table align='center' class="tablesorter" cellspacing=2 cellpadding=5 id="lab_table" border=1>
                            <thead>
                                <tr>
                                    <th>Building</th>
                                    <th>Room Number</th>
                                    <th>Discipline</th>
                                    <th>Responsible Individual</th>
                                    <th>Room Assignment</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items= "${Labs}" var="lab">

                                    <tr id="${lab.labInfoId}row">

                                        <td width="16.66%"><input type="text" id="building${lab.labInfoId}" value="${lab.building}"></td>
                                        <td width="16.66%" class="roomNumber"><input type="text" id="roomNumber${lab.labInfoId}" value="${lab.roomNumber}"></td>
                                        <td width="16.66%"><input type="text" id="discipline${lab.labInfoId}" value="${lab.discipline}"></td>
                                        <td width="16.66%"><input type="text" id="responsibleIndividual${lab.labInfoId}" value="${lab.responsibleIndividual}"></td>
                                        <td width="16.66%"><input type="text" id="roomAssignment${lab.labInfoId}" value="${lab.roomAssignment}"></td>
                                        <td width="16.66%" class="buttons"><button type="button" class="btn btn-primary" onclick="updateRowLab(${lab.labInfoId})">Update</button>
                                            <button type="button" class="btn btn-primary" onclick="deleteRowLab(${lab.labInfoId})">Delete</button>
                                        </td>
                                        <td  hidden="hidden" id="${lab.labInfoId}">${lab.labInfoId}</td>

                                    </tr>

                                </c:forEach>

                            </tbody>
                        </table>


                        <table  class="tablesorter" id="addLab" cellspacing=2 cellpadding=5 border=1 style=" overflow-x: scroll">
                            <thead></thead>
                            <tbody style="overflow:scroll">
                                <tr>

                                    <td><input placeholder="Building" type="text" id="buildingName" ></td>
                                    <td><input placeholder="Room Number" type="text" id="roomNumberNew"></td>
                                    <td><input placeholder="Discipline" type="text"  id="disciplineName"></td>
                                    <td><input placeholder="Responsible Individual" type="text" id="responsiblePerson" ></td>
                                    <td><input placeholder="Room Assignment" type="text" id="roomAssignedNew" ></td>
                                    <td><button type="button" class="btn btn-primary" onclick="addRowLab()">Add</button></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>


                <br><br>

                <%                request.getSession().setAttribute("QC", DataCRUD.getQuestionswithCategories());

                %>
                <div class="form" style=" margin-left: auto; margin-right: auto;">

                    <div align="center" class="container"  style="border:10px solid #ffffff; border-radius: 15px; background: #ffffff">

                            <table class="tablesorter" id="question_table" align='center'  border=1>
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Question</th>
                                        <th class="remove sorter-false"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items= "${QC}" var="qc">

                                        <tr id="${qc.questionId}row">
                                            <td hidden="hidden">${qc.questionId}</td>
                                            <td><input  type="text" name="categoryType" id="category${qc.questionId}" value="${qc.categoriesType}"></td>
                                            <td width="50%"><input  type="text" size="75" name="question" id="question${qc.questionId}" value="${qc.question}"></td>
                                            <td width="15.4%"><button type="button" class="btn btn-primary" onclick="updateRowQuestion(${qc.questionId})">Update</button>
                                                <button type="button" class="btn btn-primary" onclick="deleteRowQuestion(${qc.questionId})">Delete</button>
                                            </td>
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>

                            <table class="tablesorter" id="addQuestion" border=1 style="width: 100%; overflow-x: scroll">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <td style="width:32.4%">
                                            <input placeholder="Category" id="catInput" name="catName" type="text" list="catSelector"/>
                                            <datalist id="catSelector"></datalist>
                                            <button type="button" class="btn btn-primary" onclick="deleteCategory()" id="removeCategory">Delete Category</button> 
                                        </td>
                                        <td><input placeholder="Question" type="text" id="newQuestion" class="addQuestionInput" ></td>
                                        <td><button  type="button" class="btn btn-primary" onclick="addRowQuestion()">Add</button></td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </body>
</html>
