<%-- 
    Document   : Login
    Created on : Oct 25, 2016, 11:40:52 AM
    Author     : S525096
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <title>Login Page</title>
    </head>
    <body>
        <header id="top">
            <img src="css/NorthwestLogo.png" id="pic1">
            <p>Northwest Lab Inspections</p>
        </header>

        <%

            String incorrectLogin = (String) request.getAttribute("Incorrect");
            
            if (incorrectLogin == null) {
                incorrectLogin = "false";
            }

        %>

        <section>

            <form name="LoginForm" method="post" action="ValidateServlet">
                <fieldset>
                    <legend>Login</legend>
                    <% if (incorrectLogin.equals("true")) {%>
                    <font color="red">Incorrect Login Please try again</font>
                    <br />
                    <label for="name">Username:</label>
                    <br/>
                    <input type="text" name="username" id="name" required placeholder="Ex. s555555"/>
                    <br/>
                    <label for="pass">Password:</label>
                    <br/>
                    <input type="password" name="password" id="password" required/>
                    <br/>
                    <button type="Submit" id="login" value="Login">Login</button>
                    <%} else {%>
                    <label for="name">Username:</label>
                    <br/>
                    <input type="text" name="username" id="name" required placeholder="Ex. s555555"/>
                    <br/>
                    <label for="pass">Password:</label>
                    <br/>
                    <input type="password" name="password" id="password" required/>
                    <br/>
                    <button type="Submit" id="login" value="Login">Login</button>
                    <%}%>
                    &nbsp;&nbsp;
                    <button type="button" id="reg" onclick="window.location.href = 'Registration.jsp'">Register</button>


                </fieldset>
            </form>

        </section>
        <footer id="bottom">
            <p>© 2013 Northwest Missouri State University. All rights reserved.</p>
        </footer>
    </body>
</html>
