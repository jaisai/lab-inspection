<%@page import="labInspection.DataCRUD"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@page import="labInspection.ValidateServlet"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab Inspection Audit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="labInformation.js" type="text/javascript"></script>
    </head>
    <script>



        function goBack() {

            window.history.back();
        }
        function enable() {
            var person = document.getElementById("responsiblePerson");
            
            person.disabled = false;
            
        }


        $(document).ready(function () {
            $('#checkBtn').click(function () {
                checked = $("input[type=checkbox]:checked").length;

                if (!checked) {
                    alert("You must check at least one checkbox.");
                    disable();
                    return false;
                }

                if (!$('#selectRoomAssignment').val() || $('#selectRoomAssignment').val() === "Select Room Number") {
                    alert("You must select a Building, Discipline, and Room Assignment.");
                    disable();
                    return false;
                }
            });


        });
    </script>

    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                <% String fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
        </nav>




        <form action="labAuditServlet" method="post" onsubmit="enable()">
            <h2 align="center" style="gainsboro">&nbsp; &nbsp; &nbsp; &nbsp; Lab Inspection Audit</h2>
            <div class="container">
                <table align="center" style="width:75%">
                    <br/>
                    <tr>
                        <td style="width:50%;" >
                            <div style="padding-left: 15%">
                                <div class="form-group">
                                    <label style="color:gainsboro">Select the building:</label>
                                    <select class="form-control" id="building" name="building" style="width:20em">  
                                        <option value="">Select the Building</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label style="color:gainsboro">Select the discipline:</label>
                                    <select class="form-control" id="discipline" name="discipline" style="width:20em">
                                        <option value="">Select the Discipline</option>
                                    </select>
                                </div>           

                                <div class="form-group">
                                    <label style="color:gainsboro">Select Room Number:</label>
                                    <select class="form-control" id="selectRoomAssignment" name="roomNumber" style="width:20em" disabled>
                                        <option value="">Select Room Number</option>
                                    </select>
                                </div>          

                                <div class="form-group">
                                    <label style="color:gainsboro">Room Assignment: </label>
                                    <input type="text" class="form-control" style="width:20em" id="roomNumber" name="roomAssignment" disabled>        
                                </div>  

                                <div class="form-group">
                                    <label style="color:gainsboro">Responsible Person: </label>
                                    <input type="text" class="form-control" style="width:20em" id="responsiblePerson" name="responsiblePerson" disabled>        
                                </div> 
                            </div>
                        </td>
                        <td style="width:50%; padding-left: 9%;">
                            <div style="padding-top: 5%">
                                <div>
                                    <label style="color:gainsboro" for="categories">Inspection Date:</label>
                                    <p style="color:gainsboro" id="datepicker"></p>
                                    <script> document.getElementById("datepicker").innerHTML = Date();</script>
                                </div>
                                <label style="color:gainsboro" for="categories">Select the categories that apply for this room:</label>
                                <div style=" overflow-y: scroll; max-height: 260px">

                                    <div class="checkbox-group required" id="checkBoxes">
                                    </div>
                                </div><br /> 
                            </div>
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="center" colspan="2">
                            <br/>
                            <button style="width:10%" type="submit" class="btn btn-primary " id="checkBtn">Next</button>
                        </td>
                    </tr>
                </table>
            </div>

        </form>
    </body>
</html>
