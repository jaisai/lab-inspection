<%-- 
    Document   : AdminView
    Created on : Jan 25, 2017, 1:18:26 PM
    Author     : S528180
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@page import="labInspection.DataCRUD"%>
<%@page import="labInspection.LabAudit"%>
<%@page import="java.util.List"%>
<%@ page import ="java.sql.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<head>
    <title>Lab Inspection Audit</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">       
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
    <script src="css/jquery-latest.js" type="text/javascript"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="css/jquery.tablesorter_1.js" type="text/javascript"></script>
    <script src="css/jquery.tablesorter.widgets.js" type="text/javascript"></script>
    <script src="css/widget-scroller.js" type="text/javascript"></script>
    <script>

        var $table = $('#labDetails');

        function assign(id)
        {
            var edittextbox = document.getElementById("EditLabId");
            edittextbox.value = "" + id;
            document.EditLabForm.action = "EditLab";
            document.EditLabForm.submit();
        }
        //calling table sorter function
        $(document).ready(function ()
        {
            $('#labDetails').tablesorter({
                cssHeader: ".tablesorter-header",
                cssNone: ".tablesorter-headerUnSorted",
                cssAsc: ".tablesorter-headerAsc",
                cssDesc: ".tablesorter-headerDesc",
                theme: 'jui',
                sortList: [[0, 1]],
                showProcessing: true,
                headerTemplate: '{content} {icon}',
                widgets: ['uitheme', 'zebra', 'scroller'],
                widgetOptions: {
                    scroller_height: 300,
                    // scroll tbody to top after sorting
                    scroller_upAfterSort: true,
                    // pop table header into view while scrolling up the page
                    scroller_jumpToHeader: true,
                    // In tablesorter v2.19.0 the scroll bar width is auto-detected
                    // add a value here to override the auto-detected setting
                    scroller_barWidth: null
                            // scroll_idPrefix was removed in v2.18.0
                            // scroller_idPrefix : 's_'
                }


            });
        }
        );
        function call() {
            //storing year selected value
            $('#yearSelect').change(function () {
                var selectedYear = $(this).val();
                console.log(selectedYear);
                //    window.location.replace("AdminView.jsp?year=" + selectedYear);
            });
        }



        function goBack() {
            window.history.back();
        }

        function displayYears() {

            //readyState 4 = request has finished and ready 
            //staus 200 = OK (a status of 404 = page not found)
            if (requestYears.readyState === 4 && requestYears.status === 200) {
                // The split produces one extra empty string after the last newline.
                //gets data from request and puts it into yearList
                var yearList = requestYears.responseText.split("\n");
                //gets the yearsSelect tag
                var yearsSelect = document.getElementById("yearSelect");
                //adds all the options retrieved in disciplineList to discipline field
                for (var i = 0; i < yearList.length - 1; ++i) {
                    var option = document.createElement("option");
                    option.innerHTML = yearList[i];
                    option.value = yearList[i];
                    if (i === 0) {
                        option.selected = true;
                    }

                    yearsSelect.appendChild(option);
                    updateTable();
                }
            }

        }

        function updateTable() {
            requestTable = new XMLHttpRequest();
            var url = "AdminViewUpdateTable?year=" + document.getElementById("yearSelect").value;
            requestTable.open("POST", url, true);
            requestTable.onreadystatechange = displayTable;
            requestTable.send(null);
        }

        function displayTable() {

            //readyState 4 = request has finished and ready 
            //staus 200 = OK (a status of 404 = page not found)
            if (requestTable.readyState === 4 && requestTable.status === 200) {
                // The split produces one extra empty string after the last newline.
                //gets data from request and puts it into yearList
                var JSONObj = JSON.parse(requestTable.responseText);
                try {
                    $.tablesorter.clearTableBody("#labDetails");
                    $('table').trigger("update");
                } catch (err) {

                }


                //adds all the options retrieved in disciplineList to discipline field
                for (var i = 0; i < JSONObj.length; i++) {

                    var labId = JSONObj[i].labId;

                    if (JSONObj[i].status === 'Questions Unanswered') {

                        var row = '<tr><td>' + JSONObj[i].labId
                                + '</td><td>' + JSONObj[i].building
                                + '</td><td>' + JSONObj[i].roomAssignment
                                + '</td><td>' + JSONObj[i].discipline
                                + '</td><td>' + JSONObj[i].responsibleIndividual
                                + '</td><td>' + JSONObj[i].roomNumber
                                + '</td><td>' + JSONObj[i].status
                                + '</td><td class = "hidden">' + JSONObj[i].inspectionDateTable
                                + '</td><td><span class="hidden">' + JSONObj[i].inspectionDateTable + "</span>" + JSONObj[i].inspectionDate
                                + '</td><td><input type = "Submit" onclick = "finishLab(' + labId + ')" name = "' + labId + 'edit" value = "Confirm" style = "background-color:#49743D;font-weight:bold;color:#ffffff;"> </td></tr>';

                    } else {

                        var row = '<tr><td>' + JSONObj[i].labId
                                + '</td><td>' + JSONObj[i].building
                                + '</td><td>' + JSONObj[i].roomAssignment
                                + '</td><td>' + JSONObj[i].discipline
                                + '</td><td>' + JSONObj[i].responsibleIndividual
                                + '</td><td>' + JSONObj[i].roomNumber
                                + '</td><td>' + JSONObj[i].status
                                + '</td><td class = "hidden">' + JSONObj[i].inspectionDateTable
                                + '</td><td><span class="hidden">' + JSONObj[i].inspectionDateTable + "</span>" + JSONObj[i].inspectionDate
                                + '</td><td><input type = "Submit" onclick = "assign(' + labId + ')" name = "' + labId + 'edit" value = "Review" style = "background-color:#49743D;font-weight:bold;color:#ffffff;"> </td></tr>';

                    }

                    $("#labDetails").find('tbody').append(row);



                }

                $("#labDetails").find('tbody').trigger("update");

            }

        }

        function sendYearToExcel() {

            var ogYear = document.getElementById("yearSelect");
            var dupYear = document.getElementById("duplicateYearSelect");

            dupYear.value = ogYear.value;


        }

        function getYears() {
            requestYears = new XMLHttpRequest();
            var url = "AdminViewTableYears";
            requestYears.open("POST", url, true);
            requestYears.onreadystatechange = displayYears;
            requestYears.send(null);
        }

        function finishLab(id) {

            var edittextbox = document.getElementById("EditLabId");
            edittextbox.value = "" + id;
            document.EditLabForm.action = "ReturnToLab";
            document.EditLabForm.submit();

        }

        window.onload = function () {

            getYears();
        };
    </script>
    <style>
        .form {
            padding: 10px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default" style="background-color: #fffff; border: #258e12">
        <div class="container-fluid">
            <ul class ="nav navbar-nav">
                <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </button>

                <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

            </ul>
            <%String fullName = (String) request.getAttribute("fullName");%>
            <% if (fullName != "" && fullName != null) { %>
            <span class ="navbar-text">Signed in as <% out.print(fullName); %>
                <%session.setAttribute("fullName", fullName);%></span>
                <%} else { %>
                <%fullName = (String) session.getAttribute("fullName");%>
            <span class ="navbar-text">Signed in as <% out.print((String) fullName); %>
                <%session.setAttribute("fullName", fullName);%></span><% }%>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </nav>
    <div style="margin:auto" class="container">
        <div align="center">
            <h2 style="color:gainsboro">Lab Details</h2>
        </div>
        <%

            String userViewSwitch = (String) request.getAttribute("UserViewSwitch");
            String username = (String) request.getAttribute("userName");
            session.setAttribute("UserViewSwitch", userViewSwitch);
        %>
        <br>
        <br>
        <div align="center" class="row">
            <div class="col-md-3">
                <button style="width:75%" class="btn btn-primary" onclick="location.href = 'LabAudit.jsp'">Inspect Lab</button>
            </div>
            <div class="col-md-3">
                <a href="AdminTools.jsp"><button style="width:75%" type="submit" class="btn btn-primary">Add/Edit Labs</button></a>
            </div>
            <% if (userViewSwitch.equals("true")) {%>
            <div class="col-md-3">
                <form action="AdminUserSwitchServlet" method="post">
                    <input hidden="hidden" name="fromAdminView" value=1>
                    <input hidden="hidden" name="userName" value="<%=username%>">
                    <input hidden="hidden" name="fullName" value="<%=fullName%>">
                    <button style="width:75%" class="btn btn-primary" type="submit">Switch to User View</button>
                </form>
            </div>
            <%}%>

            <div class="col-md-3">

                <select style="width:75%" class="form-control" id="yearSelect" name="yearSelect" onchange="updateTable()">
                </select>
            </div>
        </div>
        <form name="EditLabForm" method="post" action="">



            <div align="center" class="container">
                <br><br>
                <div align="center" class="form" style=" border:2px solid #258e12; border-radius: 15px; background: #ffffff">
                    <table id="labDetails" class="tablesorter">
                        <thead>
                            <tr>
                                <th>Lab Id</th>
                                <th>Building</th>
                                <th>Room Assignment</th>
                                <th>Discipline</th>
                                <th>Responsible Individual</th>
                                <th>Room Number</th>
                                <th>Status</th>
                                <th>Inspection Date</th>
                                <th class="remove sorter-false"></th>
                            </tr>
                        </thead> 
                        <tbody>
                            <!-- updating from script -->
                        </tbody>
                    </table>
                    <input type="hidden" name="EditLabId" value="" id="EditLabId"/>
                </div>
            </div>


        </form>
        <form action="${pageContext.request.contextPath}/ExportExcel" method="post">
            <button type="submit" class="btn btn-primary pull-right" name="exportToExcel" onclick="sendYearToExcel()">Export to Excel</button>
            <input hidden="hidden" name="yearSelect" id="duplicateYearSelect">
        </form>


        <br>
        <!--                Room Number <input type="text"/>
                        <input type="submit" value="Filter"/>-->

    </div>
</body>