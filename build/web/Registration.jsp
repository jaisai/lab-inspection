<%-- 
    Document   : Registration
    Created on : Oct 25, 2016, 12:45:53 PM
    Author     : S525096
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Lab Redemption Registration Page</title>
    </head>
    <body style="padding-top:5%">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                



                
            </div>
        </nav>
        <script>
            
            function goBack() {
                window.history.back();
            }
            

            
            </script>
        <style>
            #header {
                color: white;
            }
            
        </style>
        <form method="post" action="RegistrationServlet" id="formId">
            <div class="container" align="center">
            <h2>Register for Lab Inspection Audit</h2>
            <div><br /></div>
            <div class="form-group">
                <label id="header" for="northwestId">Northwest Id:</label>
                <input type="text" class="form-control" style="width:20em" id="northwestId" name="northwestId" required >
            </div>
            <div class="form-group">
                <label id="header" for="firstName">First Name:</label>
                <input type="text" class="form-control" style="width:20em" id="firstName" name="firstName" required>
            </div>
            <div class="form-group">
                <label id="header" for="lastName">Last Name:</label>
                <input type="text" class="form-control" style="width:20em" id="lastName" name="lastName" required>
            </div>
            <button type="submit" class="btn btn-primary" id="submitbutton">Submit</button>
            </div>
        </form>
        </body>
   
</html>
