<%-- 
    Document   : EditLab
    Created on : Nov 7, 2016, 1:44:26 PM
    Author     : S525096
--%>

<%@page import="labInspection.DataCRUD"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<%@page import = "java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab Inspection Audit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">     
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="css/jquery.tablesorter_1.js" type="text/javascript"></script>
        <script src="css/jquery.tablesorter.widgets.js" type="text/javascript"></script>
        <script src="css/widget-scroller.js" type="text/javascript"></script>
    </head>
    <script>
        function goBack() {
            window.history.back();
        }
        $(function () {
            $("#datepicker").datepicker({
                showAnim: "slideDown",
                dateFormat: " D M dd yy"

            });

        });

        //calling table sorter function
        $(document).ready(function ()
        {
            $('#responses').tablesorter({
                cssHeader: ".tablesorter-header",
                cssNone: ".tablesorter-headerUnSorted",
                cssAsc: ".tablesorter-headerAsc",
                cssDesc: ".tablesorter-headerDesc",
                showProcessing: true,
                sortList: [[1, 0]],
                headerTemplate: '{content} {icon}',
                widgets: ['uitheme', 'zebra', 'scroller'],
                widgetOptions: {
                    scroller_height: 300,
                    // scroll tbody to top after sorting
                    scroller_upAfterSort: true,
                    // pop table header into view while scrolling up the page
                    scroller_jumpToHeader: true,
                    // In tablesorter v2.19.0 the scroll bar width is auto-detected
                    // add a value here to override the auto-detected setting
                    scroller_barWidth: null
                            // scroll_idPrefix was removed in v2.18.0
                            // scroller_idPrefix : 's_'
                }
            });

        }
        );

        function isChecked(check, id) {
            var box = document.getElementById(id);
            if (check === 'Yes') {
                box.checked = 'checked';
            }
        }




    </script>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                <% String fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
        </nav>
        <style>
            .form {
                padding: 10px;
            }
        </style>
        <form method="post" action="UpdateLabInfoServlet">
            <h2 align="center">Review Lab</h2>
            <%
                SimpleDateFormat SDF = new SimpleDateFormat(" EEE MMM dd yyyy");
                int EditLabId = (Integer) request.getAttribute("EditLabId");
                String roomNumber = (String) request.getAttribute("RoomNumber");
                String building = (String) request.getAttribute("Building");
                String Discipline = (String) request.getAttribute("Discipline");
                String ResponsibleIndividualName = (String) request.getAttribute("ResponsibleIndividualName");
                String RoomAssignment = (String) request.getAttribute("RoomAssignment");
                Date InspectionDate = SDF.parse((String) request.getAttribute("InspectionDate"));
                String formattedInspectionDate = SDF.format(InspectionDate);
                String Status = (String) request.getAttribute("Status");

            %>
            <div class="container">
                <div class="form-group">
                    <input type="hidden" class="form-control" style="width:20em" name="labId" id="labId" value="<%=EditLabId%>" >        
                </div>
                <div style="overflow-x:auto;">
                    <table class="table" style="background-color:white; border-radius: 15px;">
                        <thead>
                            <tr>
                                <th>Building</th>
                                <th>Room Number</th>
                                <th>Discipline</th>
                                <th>Responsible Individual</th>
                                <th>Room Assignment</th>
                                <th>Lab Status</th>
                                <th>Inspection Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><%=building%></td>
                                <td><%=roomNumber%></td>
                                <td><%=Discipline%></td>
                                <td><%=ResponsibleIndividualName%></td>
                                <td><%=RoomAssignment%></td>
                                <td><%=Status%><input hidden="hidden" name="Status" value="<%=Status%>"</td>
                                <td><input type="text" class="form-control" readonly="readonly" style="width:20em" name="inspectionDate" id="datepicker" value="<%=formattedInspectionDate%>" style="background: white;"></td>
                            </tr>
                        </tbody>   
                    </table>
                </div>
            </div>
            <%

                request.getSession().setAttribute("editTableLabs", DataCRUD.editLabTable(EditLabId));
            %>

            <div class="container">
                <h2 align="center">Responses</h2>
                <div align="center" class="form" style=" border:2px solid #258e12; border-radius: 15px; width: auto; background: #ffffff">
                    <table id="responses" class="tablesorter">
                        <thead>
                        <th>Question</th>
                        <th>Compliant</th>
                        <th>Original Comments</th>
                        <th>Faculty Comments</th>
                        <th>Reviewer Comments</th>
                        </thead>

                        <tbody>

                            <c:forEach  items="${editTableLabs}" var="lab">

                                <tr>
                                    <td id="question">${lab.question}</td>
                                    <td id="compliance" name="compliance">
                                        <%if (Status.equals("Done")) {%>
                                        <c:if test="${lab.compliant == 'Yes'}">
                                            <span hidden="hidden">Yes</span>
                                            <label><input disabled="true" type="checkbox" id="${lab.questionId}" checked="checked"  name="${lab.questionId}" >Yes</label>
                                            </c:if> 
                                            <c:if test="${lab.compliant == 'No'}">

                                            <label><input disabled="true" type="checkbox" id="${lab.questionId}"  name="${lab.questionId}" >Yes</label>
                                            </c:if> 
                                            <%} else { %>
                                            <c:if test="${lab.compliant == 'Yes'}">
                                            <span hidden="hidden">Yes</span>
                                            <label><input type="checkbox" id="${lab.questionId}" checked="checked"  name="${lab.questionId}" >Yes</label>
                                            </c:if> 
                                            <c:if test="${lab.compliant == 'No'}">

                                            <label><input type="checkbox" id="${lab.questionId}"  name="${lab.questionId}" >Yes</label>
                                            </c:if> 
                                            <%}%>
                                    </td>
                                    <%if (Status.equals("Waiting to Send")) {%>
                                    <td id="originalComments">
                                        <input name="originalCommentsNew" type="text" value="${lab.originalComments}">
                                        <input name="originalCommentsNewOrder" hidden="hidden" type="text" value="${lab.questionId}">
                                    </td>
                                    <%} else {%>
                                    <td id="originalComments" name="originalComments">
                                        ${lab.originalComments}
                                        <input hidden="hidden" name="originalComments" value="${lab.originalComments}">
                                        <input name="originalCommentsOrder" hidden="hidden" type="text" value="${lab.questionId}">

                                    </td>

                                    <%}%>
                                    <td id="facultyComments">${lab.facultyComments}</td>

                                    <%if (Status.equals("Waiting to Send")) {%>
                                    <td id="adminComments"><span>${lab.adminComments}</span>
                                        <input hidden="hidden" name="adminComments" value="${lab.adminComments}">
                                        <input name="adminCommentsOrder" hidden="hidden" type="text" value="${lab.questionId}">
                                    </td>

                                    <%} else {%>
                                    <td id="adminComments">
                                        <%if (Status.equals("Done")) {%>
                                        <span>${lab.adminComments}</span>
                                        <br/>
                                        <input hidden="hidden" name="adminComments" value="${lab.adminComments}">
                                        <input name="adminCommentsOrder" hidden="hidden" type="text" value="${lab.questionId}">
                                        <%} else {%>
                                        <span>${lab.adminComments}</span>
                                        <br/>
                                        <input name="adminCommentsNew" type="text">
                                        <input hidden="hidden" name="adminComments" value="${lab.adminComments}">
                                        <input name="adminCommentsOrder" hidden="hidden" type="text" value="${lab.questionId}">
                                        <%}%>
                                    </td>
                                    <%}%>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <div class="container" align="center">
                <button style="width:10%; margin-right:5%" type="submit" name="NeedConfirmation" class="btn btn-primary ">Save</button>
                <%if (!Status.equals("Done")) {%>
                <button style="width:10%" type="submit" name="SendToFaculty" class="btn btn-primary ">Send</button>
                <%}%>
            </div>

        </form>             
    </body>
</html>
