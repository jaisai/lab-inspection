/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getRoomAssignment() { 
    clearRoomAssignmentData();
    
    request = new XMLHttpRequest();
    //creates url to call RoomAssignmentServlet
    var url = "RoomAssignmentServlet?building=" + document.getElementById("building").value + "&discipline=" + document.getElementById("discipline").value;
    request.open("POST", url, true);
    //when onreadystate changes call displayRoomassignments()
    request.onreadystatechange = displayRoomAssignments;
    request.send(null);
}

function displayRoomAssignments() {
    //readyState 4 = request has finished and ready 
    //staus 200 = OK (a status of 404 = page not found)
    if (request.readyState === 4 && request.status === 200) {
        // The split produces one extra empty string after the last newline.
        //gets data from request and puts it into assignmentsList
        var assignmentsList = request.responseText.split("\n");

        //enables the selectRoomAssignment select field
        document.getElementById("selectRoomAssignment").disabled = false;
        //gets the selectRoomAssignment field
        var select = document.getElementById("selectRoomAssignment");
        
        //adds all the options retrieved in assignmentsList to selectRoomAssignment
        for (var i = 0; i < assignmentsList.length - 1; ++i) {  
            var option = document.createElement("option");
            option.value = assignmentsList[i];
            option.innerHTML = assignmentsList[i];
            select.appendChild(option);
        }
    }
}

function clearRoomAssignmentData() {
    var select = document.getElementById("selectRoomAssignment");
    for(var i=select.options.length-1;i>=0;i--)
    {
	select.remove(i);
    }
    var option = document.createElement("option");
    option.value = "Select Room Assignment";
    option.innerHTML = "Select Room Assignment";
    select.appendChild(option);
}

function getRoomNumber() { 
    clearRoomNumbersData();
    
    request = new XMLHttpRequest();
    var url = "RoomAssignmentServlet?building=" + document.getElementById("building").value + "&discipline=" + document.getElementById("discipline").value + "&selectRoomAssignment=" + document.getElementById("selectRoomAssignment").value;    
//    window.alert(url);
    request.open("POST", url, true);
    request.onreadystatechange = displayRoomNumbers;
    request.send(null);
}


function displayRoomNumbers() {
    if (request.readyState === 4 && request.status === 200) {
        // The split produces one extra empty string after the last newline.
        var roomList = request.responseText.split("\n");
//        window.alert(roomList);
        document.getElementById("roomNumber").disabled = false;
        document.getElementById("responsiblePerson").disabled = false;
        
        var roomNumber = document.getElementById("roomNumber");
        var responsiblePerson = document.getElementById("responsiblePerson");
                
        var i = roomList.length-1;

        roomNumber.value = roomList[i-2];
        responsiblePerson.value = roomList[i-1];
    }
}

function clearRoomNumbersData() {
    document.getElementById("roomNumber").innerHTML = "";
    document.getElementById("roomNumber").disabled = true;
    document.getElementById("responsiblePerson").innerHTML = "";
    document.getElementById("responsiblePerson").disabled = true;
}

function getBuildings() {
    clearBuildingData();
    
    request = new XMLHttpRequest();
    //creates url to call RoomAssignmentServlet
    var url = "RoomAssignmentServlet";
    request.open("POST", url, true);
    //when onreadystate changes call displayRoomassignments()
    request.onreadystatechange = displayBuildings;
    request.send(null);
}

function displayBuildings() {
    //readyState 4 = request has finished and ready 
    //staus 200 = OK (a status of 404 = page not found)
    if (request.readyState === 4 && request.status === 200) {
        // The split produces one extra empty string after the last newline.
        //gets data from request and puts it into buildingList
        var buildingList = request.responseText.split("\n");
        
        //gets the Building field
        var select = document.getElementById("building");
        
        //adds all the options retrieved in buildingList to building field
        for (var i = 0; i < buildingList.length - 1; ++i) {  
            var option = document.createElement("option");
            option.value = buildingList[i];
            option.innerHTML = buildingList[i];
            select.appendChild(option);
        }
    }
}

function clearBuildingData() {
    var select = document.getElementById("building");
    for(var i=select.options.length-1;i>=0;i--)
    {
	select.remove(i);
    }
    var option = document.createElement("option");
    option.value = "Select the Building";
    option.innerHTML = "Select the Building";
    select.appendChild(option);
}

function getDisciplines() {
    clearDisciplineData();
    
    request = new XMLHttpRequest();
    //creates url to call RoomAssignmentServlet
    var url = "RoomAssignmentServlet?building=" + document.getElementById("building").value;
    request.open("POST", url, true);
    //when onreadystate changes call displayRoomassignments()
    request.onreadystatechange = displayDisciplines;
    request.send(null);
}

function displayDisciplines() {
    //readyState 4 = request has finished and ready 
    //staus 200 = OK (a status of 404 = page not found)
    if (request.readyState === 4 && request.status === 200) {
        // The split produces one extra empty string after the last newline.
        //gets data from request and puts it into disciplineList
        var disciplineList = request.responseText.split("\n");

        //gets the discipline field
        var select = document.getElementById("discipline");
        
        //adds all the options retrieved in disciplineList to discipline field
        for (var i = 0; i < disciplineList.length - 1; ++i) {  
            var option = document.createElement("option");
            option.value = disciplineList[i];
            option.innerHTML = disciplineList[i];
            select.appendChild(option);
        }
    }
}

function clearDisciplineData() {
    var select = document.getElementById("discipline");
    for(var i=select.options.length-1;i>=0;i--)
    {
	select.remove(i);
    }
    var option = document.createElement("option");
    option.value = "Select the Discipline";
    option.innerHTML = "Select the Discipline";
    select.appendChild(option);
}

window.onload = function() {
//    document.getElementById("getDetails").onclick = getRoomAssignment;
    
    getBuildings();
    document.getElementById("building").onchange = getDisciplines;
    document.getElementById("discipline").onchange = getRoomAssignment;
    document.getElementById("selectRoomAssignment").onchange = getRoomNumber;
    
};

