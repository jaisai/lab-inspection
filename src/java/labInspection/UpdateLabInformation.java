/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author s515739
 */
@WebServlet(name = "UpdateLabInformation", urlPatterns = {"/UpdateLabInformation"})
public class UpdateLabInformation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int updateId = Integer.parseInt(request.getParameter("updateRow"));
        int labInfoId;
        String building;
        String roomNumber;
        String discipline;
        String responsibleIndividual;
        String roomAssignment;
        String categoryName;

        int questionId;
        String question;
        int categoryId;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UpdateLabInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            switch (updateId) {
                case 1:
                    //updating labs
                    labInfoId = Integer.parseInt(request.getParameter("labInfoId"));
                    building = request.getParameter("building");
                    roomNumber = request.getParameter("roomNumber");
                    discipline = request.getParameter("discipline");
                    responsibleIndividual = request.getParameter("responsibleIndividual");
                    roomAssignment = request.getParameter("roomAssignment");

                    updateLabInformation(con, ps, labInfoId, building, roomNumber, discipline, responsibleIndividual, roomAssignment);

                    break;
                case 2:
                    //updating questions
                    questionId = Integer.parseInt(request.getParameter("questionId"));
                    question = request.getParameter("question");
                    categoryId = Integer.parseInt(request.getParameter("categoryId"));

                    updateQuestionInformation(con, ps, questionId, question, categoryId);

                    break;
                case 3: //adding lab

                    building = request.getParameter("building");
                    roomNumber = request.getParameter("roomNumber");
                    discipline = request.getParameter("discipline");
                    responsibleIndividual = request.getParameter("responsibleIndividual");
                    roomAssignment = request.getParameter("roomAssignment");

                    int newLabId = addRowLab(con, ps, building, roomNumber, discipline, responsibleIndividual, roomAssignment);
                    try (PrintWriter out = response.getWriter()) {
                        out.print(newLabId);
                    }

                    break;
                case 4: //adding question

                    question = request.getParameter("question");
                    categoryName = request.getParameter("categoryName");

                    int returnQuestionId = addRowQuestion(con, ps, question, categoryName);
                    try (PrintWriter out = response.getWriter()) {
                        out.print(returnQuestionId);
                    }

                    break;
                case 5: //deleting lab
                    labInfoId = Integer.parseInt(request.getParameter("labInfoId"));

                    deleteRowLab(con, ps, labInfoId);

                    break;
                case 6: //deleting question

                    questionId = Integer.parseInt(request.getParameter("questionId"));

                    deleteRowQuestion(con, ps, questionId);

                    break;
                case 7: //deleting category
                    categoryName = request.getParameter("categoryName");
                    deleteCategory(con, ps, categoryName);

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UpdateLabInformation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UpdateLabInformation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public static void deleteCategory(Connection con, PreparedStatement ps, String category) throws ClassNotFoundException, SQLException {
        String query = "Select categoriesId FROM categories WHERE categoriesType = '" + category + "'";
        ps = con.prepareStatement(query);
        int catId;
        try (ResultSet catRS = ps.executeQuery()) {
            catRS.next();
            catId = catRS.getInt(1);
            catRS.close();
        }

        query = "DELETE FROM questions WHERE categoryId = " + catId;
        ps = con.prepareStatement(query);
        ps.executeUpdate();
        

        query = "DELETE FROM categories WHERE categoriesType = '" + category + "'";

        ps = con.prepareStatement(query);
        ps.executeUpdate();
        
        con.close();
        ps.close();
    }

    public static void updateLabInformation(Connection con, PreparedStatement ps, int labInfoId, String building, String roomNumber, String discipline, String responsibleIndividual, String roomAssignment) throws ClassNotFoundException, SQLException {
        String query = "UPDATE labinformation set Building = '" + building + "', RoomNumber = '" + roomNumber + "', Discipline = '" + discipline + "', ResponsiblePerson = '" + responsibleIndividual + "', Assignment = '" + roomAssignment + "' WHERE LabInfoId = " + labInfoId;

        ps = con.prepareStatement(query);
        ps.executeUpdate();
        ps.close();
        con.close();
    }

    public static void updateQuestionInformation(Connection con, PreparedStatement ps, int questionId, String question, int categoryId) throws ClassNotFoundException, SQLException {
        String query = "UPDATE questions set question = " + question + ", categoryId = " + categoryId + ", WHERE questionId = " + questionId;

        ps = con.prepareStatement(query);
        ps.executeUpdate();
        ps.close();
        con.close();
    }

    public static int addRowLab(Connection con, PreparedStatement ps, String building, String roomNumber, String discipline, String responsibleIndividual, String roomAssignment) throws ClassNotFoundException, SQLException {

        String query = "INSERT INTO labinformation (Building, Discipline, RoomNumber, Assignment, ResponsiblePerson) VALUES ('" + building + "', '" + discipline + "', '" + roomNumber + "', '" + roomAssignment + "', '" + responsibleIndividual + "');";

        ps = con.prepareStatement(query);
        ps.executeUpdate();

        query = "SELECT MAX(labInfoId) from labinformation";
        
        ps = con.prepareStatement(query);
        ResultSet labIdRS = ps.executeQuery();
        labIdRS.next();
        return labIdRS.getInt(1);

    }

    public static void deleteRowLab(Connection con, PreparedStatement ps, int labInfoId) throws ClassNotFoundException, SQLException {

        String query = "DELETE FROM labinformation WHERE LabInfoId = " + labInfoId;

        ps = con.prepareStatement(query);
        ps.executeUpdate();
        ps.close();
        con.close();
    }

    public static int addRowQuestion(Connection con, PreparedStatement ps, String question, String categoryName) throws ClassNotFoundException, SQLException {
        List<Categories> catList = DataCRUD.getCategories();

        int newCat = 1;
        int catId = 0;
        String query;

        //checks if the category is a new category
        for (Categories catList1 : catList) {
            if (catList1.getCategoriesType().equals(categoryName)) {
                newCat = 0;
                catId = catList1.getCategoriesId();
            }
        }

        //if the category is new, adds it to database and gets new categoryId
        if (newCat == 1) {
            query = "INSERT INTO categories (categoriesType) values ('" + categoryName + "')";

            ps = con.prepareStatement(query);
            ps.executeUpdate();

            query = "SELECT categoriesId FROM categories WHERE categoriesType = '" + categoryName + "'";
            
            ps = con.prepareStatement(query);
            try (ResultSet catIdRS = ps.executeQuery()) {
                catIdRS.next();
                
                catId = catIdRS.getInt(1);
                
                catIdRS.close();
            }
        }

        //inserts question into database
        query = "INSERT INTO questions (question, categoryId) values ('" + question + "', " + catId + ")";
        ps = con.prepareStatement(query);
        ps.executeUpdate();

        query = "SELECT questionId FROM questions WHERE question = '" + question + "' AND categoryId = " + catId;
        ps = con.prepareStatement(query);
        ResultSet questionIdRS = ps.executeQuery();

        questionIdRS.next();
        return questionIdRS.getInt(1);
    }

    public static void deleteRowQuestion(Connection con, PreparedStatement ps, int questionId) throws ClassNotFoundException, SQLException {

        String query = "DELETE FROM questions WHERE questionId = " + questionId;

        ps = con.prepareStatement(query);
        ps.executeUpdate();
        ps.close();
        con.close();
    }

}
