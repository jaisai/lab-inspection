/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

/**
 *
 * @author s515739
 */
public class EditLabTable {
    private String question;
    private int questionId;
    private String compliant;
    private String originalComments;
    private String facultyComments;
    
    private String adminComments;

    public EditLabTable(String question, int questionId, String compliant, String originalComments, String facultyComments, String adminComments) {
        this.question = question;
        this.questionId = questionId;
        this.compliant = compliant;
        this.originalComments = originalComments;
        this.facultyComments = facultyComments;
       
        this.adminComments = adminComments;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    
    
    public String getOriginalComments() {
        return originalComments;
    }

    public void setOriginalComments(String originalComments) {
        this.originalComments = originalComments;
    }

    
    
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCompliant() {
        return compliant;
    }

    public void setCompliant(String compliant) {
        this.compliant = compliant;
    }

    public String getFacultyComments() {
        return facultyComments;
    }

    public void setFacultyComments(String facultyComments) {
        this.facultyComments = facultyComments;
    }

    public String getAdminComments() {
        return adminComments;
    }

    public void setAdminComments(String adminComments) {
        this.adminComments = adminComments;
    }
    
    
}
