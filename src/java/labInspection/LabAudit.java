package labInspection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author S525096
 */
public class LabAudit {
    private String roomNumber;
    private String building;
    private String discipline;
    private String responsibleIndividualName;
    private String roomAssignment;
    private Date inspectionDate;
    private Date inspectionDateTable;
    private String formattedInspectionDate;
    private String status;
    private int labId;

    public LabAudit(int labId, String building, String roomNumber, String discipline, String responsibleInduvidualName, String roomAssignment, Date inspectionDate, String status) {
        SimpleDateFormat SDF = new SimpleDateFormat(" EEE MMM dd yyyy");
        this.roomNumber = roomNumber;
        this.building = building;
        this.discipline = discipline;
        this.responsibleIndividualName = responsibleInduvidualName;
        this.roomAssignment = roomAssignment;
        this.inspectionDate = inspectionDate;
        this.status = status;
        this.labId = labId;
        inspectionDateTable = this.inspectionDate;
        formattedInspectionDate = SDF.format(inspectionDate);
    }
    
     public LabAudit(String building, String roomNumber, String discipline, String responsibleInduvidualName, String roomAssignment, Date inspectionDate, String status) {
        SimpleDateFormat SDF = new SimpleDateFormat(" EEE MMM dd yyyy");
        this.roomNumber = roomNumber;
        this.building = building;
        this.discipline = discipline;
        this.responsibleIndividualName = responsibleInduvidualName;
        this.roomAssignment = roomAssignment;
        this.inspectionDate = inspectionDate;
        this.status = status;
        inspectionDateTable = this.inspectionDate;
        formattedInspectionDate = SDF.format(inspectionDate);
    }

    public String getFormattedInspectionDate() {
        return formattedInspectionDate;
    }

    public void setFormattedInspectionDate(String formattedInspectionDate) {
        this.formattedInspectionDate = formattedInspectionDate;
    }



    public LabAudit() {
    }

    public Date getInspectionDateTable() {
        return inspectionDateTable;
    }

    public void setInspectionDateTable(Date inspectionDateTable) {
        this.inspectionDateTable = inspectionDateTable;
    }
    
    
    
    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getResponsibleIndividualName() {
        return responsibleIndividualName;
    }

    public void setResponsibleIndividualName(String responsibleInduvidualName) {
        this.responsibleIndividualName = responsibleInduvidualName;
    }

    public String getRoomAssignment() {
        return roomAssignment;
    }

    public void setRoomAssignment(String roomAssignment) {
        this.roomAssignment = roomAssignment;
    }

    public String getInspectionDate() {
        DateFormat SDF = new SimpleDateFormat(" EEE MMM dd yyyy");
        return SDF.format(inspectionDate);
    }

    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getLabId() {
        return labId;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }

    @Override
    public String toString() {
        return "LabAudit{" + "roomNumber=" + roomNumber + ", building=" + building + ", discipline=" + discipline + ", responsibleIndividualName=" + responsibleIndividualName + ", roomAssignment=" + roomAssignment + ", inspectionDate=" + inspectionDate + ", inspectionDateTable=" + inspectionDateTable + ", formattedInspectionDate=" + formattedInspectionDate + ", status=" + status + ", labId=" + labId + '}';
    }
    
    
    
}
