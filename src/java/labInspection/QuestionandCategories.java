/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

/**
 *
 * @author s528180
 */
public class QuestionandCategories {
     private int questionId;
     private String question;
     private int categoryId;
     private String categoriesType;

    public QuestionandCategories(int questionId,String categoriesType, String question) {
        this.questionId = questionId;
        this.question = question;
        this.categoriesType = categoriesType;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoriesType() {
        return categoriesType;
    }

    public void setCategoriesType(String categoriesType) {
        this.categoriesType = categoriesType;
    }
     
     
}
