package labInspection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author S525096
 */
public final class Responses {

    //private int responseID;

    private int labID;
    private int categoryId;
    private String question;
    private String compliant;
    private String comments;
    private int questionId;

    public Responses(int labID, int categoryId, String question, String compliant, String comments) throws SQLException {
        this.labID = labID;
        this.categoryId = categoryId;
        this.question = question;
        this.compliant = compliant;
        this.comments = comments;
        this.questionId = getQuestionIdFromDB(question);
    }

    public Responses(int labID, String question, int questionId, String compliant, String comments) {
        this.labID = labID;
        this.question = question;
        this.questionId = questionId;
        this.compliant = compliant;
        this.comments = comments;
    }

    public int getQuestionIdFromDB(String question) throws SQLException {

        String query = "SELECT questionId from questions where question = '" + question + "'";

        ResultSet questionIdRS = null;
        Connection con = null;
        PreparedStatement ps = null;
        int id = 0;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            questionIdRS = ps.executeQuery();
            questionIdRS.next();
            id = questionIdRS.getInt(1);
            questionIdRS.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Responses.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                con.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (questionIdRS != null) {
                questionIdRS.close();
            }
        }
        return id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getLabID() {
        return labID;
    }

    public void setLabID(int labID) {
        this.labID = labID;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCompliant() {
        return compliant;
    }

    public void setCompliant(String compliant) {
        this.compliant = compliant;
    }

}
