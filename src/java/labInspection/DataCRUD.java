package labInspection;

import java.io.File;
import java.io.IOException;
import javax.mail.*;
import javax.mail.internet.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.write.*;
import jxl.write.Number;
import jxl.*;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author S525096
 */
public class DataCRUD {

    public int addNewLab(LabAudit labaudit) { //adds new lab to databse and returns the labId
        int id = 0;
        java.sql.Date dateString = new java.sql.Date(labaudit.getInspectionDateTable().getTime());
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "insert into labdetails(building, roomNUmber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status) values(?,?,?,?,?,?,?)";
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.setString(1, labaudit.getBuilding());
            ps.setString(2, labaudit.getRoomNumber());
            ps.setString(3, labaudit.getDiscipline());
            ps.setString(4, labaudit.getResponsibleIndividualName());
            ps.setString(5, labaudit.getRoomAssignment());
            ps.setDate(6, dateString);
            ps.setString(7, labaudit.getStatus());
            ps.executeUpdate();

            query = "select labId from labdetails where building = '" + labaudit.getBuilding() + "' && roomNumber = '" + labaudit.getRoomNumber() + "' && discipline = '" + labaudit.getDiscipline() + "' && responsibleIndividual = '" + labaudit.getResponsibleIndividualName() + "' && roomAssignment = '" + labaudit.getRoomAssignment() + "';";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return id;
    }

    public static ArrayList<Labs> getLabInformation() throws ClassNotFoundException, SQLException {
        LabInformation labInfo = new LabInformation();

        return labInfo.getLabs();
    }

    public static ArrayList<Questions> getQuestionInformation() throws ClassNotFoundException { //returns an array of questions which contains the questionId, question, and categoryId

        String query = "SELECT * FROM questions";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet questionRS = null;
        ArrayList<Questions> questionArray = new ArrayList<>();
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            questionRS = ps.executeQuery();

            while (questionRS.next()) {

                questionArray.add(new Questions(questionRS.getInt(1), questionRS.getString(2), questionRS.getInt(3)));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (questionRS != null) {
                    questionRS.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return questionArray;
    }

    public void addNewLabResponses(Responses response) { //adds a new response to the databse; used when creating a new lab inspection
        Connection con = null;
        PreparedStatement ps = null;
        String query = "insert into responses(labId, questionId, compliant, comments) values(?,?,?,?)";
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, response.getLabID());
            ps.setInt(2, response.getQuestionId());
            ps.setString(3, response.getCompliant());
            ps.setString(4, response.getComments());
            ps.executeUpdate();

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateLabResponses(Responses response) {
        int labId = response.getLabID();
        int questionId = response.getQuestionId();
        Connection con = null;
        PreparedStatement ps = null;

        String query = "UPDATE responses SET compliant = '" + response.getCompliant() + "', comments = '" + response.getComments() + "' WHERE labId = " + labId + " AND questionId = " + questionId;

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static List<Categories> getCategories() {
        List<Categories> ls = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = null;
        String query = "select * from categories";
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Categories categories = new Categories(rs.getInt(1), rs.getString(2));
                ls.add(categories);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static List<Questions> getQuestions(String categories) {
        List<Questions> ls = new LinkedList<>();
        String query = "select * from questions where categoryId in (" + categories + ")";
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Questions questions = new Questions(rs.getInt(1), rs.getString(2), rs.getInt(3));
                System.out.println(questions.toString());
                ls.add(questions);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static ArrayList<String> getQuestionCategories() throws ClassNotFoundException {
        ArrayList<String> categoryNames = new ArrayList<>();

        String query = "SELECT categoriesType FROM categories";
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet catRS = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            catRS = ps.executeQuery();
            while (catRS.next()) {
                categoryNames.add(catRS.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (catRS != null) {
                    catRS.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return categoryNames;
    }

    public static List<Integer> getQuestionIds(String categories) {
        List<Integer> ls = new ArrayList<>();
        String query = "select questionId from questions where categoryId in (" + categories + ")";
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ls.add(rs.getInt(1));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, e + " null prepared statement or result set");
            }

        }

        return ls;
    }

    public static List<Integer> getYears() { //gets every unique year from the dates of inspections
        List<Integer> ls = new ArrayList<>();
        String query = "select distinct year(inspectionDate) from labdetails ORDER BY inspectiondate DESC";
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ls.add(rs.getInt(1));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return ls;
    }

    public static List<LabAudit> getLabDetails() {
        List<LabAudit> ls = new ArrayList<>();
        String query = "select labId, building, roomNumber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status from labdetails";
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                LabAudit labAudit = new LabAudit(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDate(7), rs.getString(8));

                ls.add(labAudit);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static List<LabAudit> getLabDetailsFromYear(int year) {
        List<LabAudit> ls = new ArrayList<>();
        String query = "select labId, building, roomNumber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status from labdetails where year(inspectionDate)=" + year;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                LabAudit labAudit = new LabAudit(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDate(7), rs.getString(8));

                ls.add(labAudit);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static LabAudit getLabDetailsFromLabId(int labId) {
        LabAudit ls = new LabAudit();
        String query = "select labId, building, roomNumber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status from labdetails where labId = '" + labId + "'";
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                ls = new LabAudit(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDate(7), rs.getString(8));

                System.out.println(ls.toString());

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static LabAudit getLabDetailsFromLabIdUserView(int labId) {
        LabAudit ls = new LabAudit();
        String query = "select labId, building, roomNumber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status from labdetails where labId = '" + labId + "' AND (status = 'In Progress' OR status = 'Need Faculty Confirmation')";
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                ls = new LabAudit(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDate(7), rs.getString(8));

                System.out.println(ls.toString());

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

    public static ArrayList<Responses> getResponsesFromLabId(int labId) {
        ArrayList<Responses> responses = new ArrayList<>();
        String query = "select labId, questions.categoryId, question, compliant, comments from responses,questions where responses.questionId = questions.questionId and labId = '" + labId + "'";
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Responses res = new Responses(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5));
                responses.add(res);

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return responses;
    }

    public static File generateReports(int year) throws IOException, WriteException, ClassNotFoundException, SQLException {

        //creates file for excel document
        //this location is temporary and will not be used
        File file = new File("C:\\LabReport" + year + ".xls");

        //creating the excel document
        WritableWorkbook workbook = Workbook.createWorkbook(file);
        //name of sheet, place in order
        WritableSheet compliantSummary = workbook.createSheet("Compliant Summary", 2);
        WritableSheet summary = workbook.createSheet("Summary", 1);
        WritableSheet nonCompliantIncidentsSheet = workbook.createSheet("Non-Compliant Incidents", 0);
        WritableSheet rawDataSheet = workbook.createSheet("Raw Data", 3);

        workbook = excelCreation(workbook, year);

        workbook.write();
        workbook.close();

        //returns file to servlet for user download
        return file;
    }

    private static WritableWorkbook excelCreation(WritableWorkbook workbook, int year) throws WriteException, ClassNotFoundException {
        //retrieve sheets from workbook
        WritableSheet rawDataSheet = workbook.getSheet("Raw Data");
        WritableSheet nonCompliantSheet = workbook.getSheet("Non-Compliant Incidents");
        WritableSheet summarySheet = workbook.getSheet("Summary");
        WritableSheet compliantSummarySheet = workbook.getSheet("Compliant Summary");

        //creates format for bolded titles
        WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 10);
        titleFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat titleFormat = new WritableCellFormat(titleFont);

        //Creates format for centered cells
        WritableCellFormat centerFormat = new WritableCellFormat();
        centerFormat.setAlignment(jxl.format.Alignment.CENTRE);

        //Wrap Text format
        WritableCellFormat wrapFormat = new WritableCellFormat();
        wrapFormat.setWrap(true);

        //just a Bold Font and format
        WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10);
        boldFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat boldFormat = new WritableCellFormat(boldFont);
        boldFormat.setAlignment(jxl.format.Alignment.CENTRE);

        workbook = headerCreation(workbook, titleFormat, year);

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet questionNameRS = null;
        ResultSet totalLabsRS = null;
        ResultSet totalLabsNotDONERS = null;
        ResultSet questionRS = null;
        ResultSet returnedRS = null;
        ResultSet nonCompRS = null;
        ResultSet nonCompliantRS = null;
        ResultSet compliantRS = null;
        ResultSet categoryRS = null;

        try {
            con = DatabaseConnection.getConnection();

            //query for the question names
            String questionNameQuery = "SELECT question, categoryId FROM labinspection.questions"; //Names of each question
            ps = con.prepareStatement(questionNameQuery);
            questionNameRS = ps.executeQuery();
            String questionName;

            //Number of labs left to inspect
            String totalLabs = "SELECT count(labId) FROM labdetails WHERE year(inspectionDate) = " + year;//number of labs from year
            String totalLabsNotDONE = "SELECT count(labId) FROM labdetails WHERE status != 'Done' AND year(inspectionDate) = " + year; //number of labs from year that are not done
            ps = con.prepareStatement(totalLabs);
            totalLabsRS = ps.executeQuery();

            ps = con.prepareStatement(totalLabsNotDONE);
            totalLabsNotDONERS = ps.executeQuery();

            totalLabsRS.next();
            totalLabsNotDONERS.next();

            int totalLabsNum = totalLabsRS.getInt(1);
            int labsNotReturned = totalLabsNotDONERS.getInt(1);

            //creates font for redFormat which makes the words that use this format red and bold
            WritableFont redFont = new WritableFont(WritableFont.ARIAL, 10);
            redFont.setBoldStyle(WritableFont.BOLD);
            redFont.setColour(jxl.format.Colour.RED);

            //Adds red text that says how many labs have not been finished on each page
            //create redFormat (red text)
            WritableCellFormat redFormat = new WritableCellFormat();
            redFormat.setFont(redFont); //sets font of cell format
            rawDataSheet.addCell(new Label(0, 3, "A total of " + totalLabsNum + " labs were inspected of which " + labsNotReturned + " reports were not returned", redFormat));
            nonCompliantSheet.addCell(new Label(0, 3, "A total of " + totalLabsNum + " labs were inspected of which " + labsNotReturned + " reports were not returned", redFormat));
            summarySheet.addCell(new Label(0, 3, "A total of " + totalLabsNum + " labs were inspected of which " + labsNotReturned + " reports were not returned", redFormat));
            compliantSummarySheet.addCell(new Label(0, 16, "A total of " + totalLabsNum + " labs were inspected of which " + labsNotReturned + " reports were not returned", redFormat));

            NumberFormat percent = new NumberFormat("#%");

            WritableCellFormat percentFormat = new WritableCellFormat(percent);
            percentFormat.setFont(boldFont);
            compliantSummarySheet.addCell(new Label(0, 17, "Inspection Reports Not Returned", titleFormat));
            float complianceRatePercent = ((float) labsNotReturned / totalLabsNum);
            compliantSummarySheet.addCell(new Number(1, 17, complianceRatePercent, percentFormat));

            //loop for question names
            int j = 1;
            int lastQuestionId;

            int loopCounter = 1;

            String catQuery = "SELECT categoriesId, categoriesType FROM categories";
            ps = con.prepareStatement(catQuery);
            categoryRS = ps.executeQuery();
            HashMap<Integer, String> categoryList = new HashMap<>();

            /*
             This gets the first category id from the database then
             sets the result set back to the beginning for when
             it fills the categoryList
             */
            categoryRS.next();
            int currentCategoryId = categoryRS.getInt(1);
            int firstCategoryId = currentCategoryId;
            categoryRS.beforeFirst();

            while (categoryRS.next()) {
                categoryList.put(categoryRS.getInt(1), categoryRS.getString(2));
            }

            rawDataSheet.addCell(new Label(0, (j + 4), categoryList.get(currentCategoryId), titleFormat));

            while (questionNameRS.next()) {
                //puts lastQuestionId becomes the id used on previous loop
                lastQuestionId = currentCategoryId;
                //get next questionId
                currentCategoryId = questionNameRS.getInt(2);
                //If they match than the next question is a part of the same category as the last one
                //If they do NOT match then the question's categories are different
                if (lastQuestionId == currentCategoryId) {

                    questionName = questionNameRS.getString(1);

                    rawDataSheet.addCell(new Label(0, (j + 5), questionName, wrapFormat));
                    j++;

                } else {
                    j += 2;

                    //prints category header for each category except general safety which will always be printed first
                    rawDataSheet.addCell(new Label(0, (j + 4), categoryList.get(currentCategoryId), titleFormat));
                    loopCounter++;

                    questionName = questionNameRS.getString(1);

                    rawDataSheet.addCell(new Label(0, (j + 5), questionName, wrapFormat));
                    j++;
                }

            }
            j = 1;
            // Number of labs that have each question (labs Inspected column)
            String numOfQuestionsQuery = "SELECT questions.questionId, count(labdetails.labId), categoryId FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND year(inspectionDate) = " + year + " group by questionId";
            ps = con.prepareStatement(numOfQuestionsQuery);
            questionRS = ps.executeQuery();

            int numOfLabs;
            //set to 101 because that is the first category id
            currentCategoryId = firstCategoryId;
            while (questionRS.next()) {
                // same as above this process repeats for each page that needs to print out each question
                lastQuestionId = currentCategoryId;
                currentCategoryId = questionRS.getInt(3);
                if (lastQuestionId == currentCategoryId) {
                    numOfLabs = questionRS.getInt(2);

                    rawDataSheet.addCell(new Number(1, (j + 5), numOfLabs, centerFormat));
                    j++;
                } else {
                    j += 2;
                    numOfLabs = questionRS.getInt(2);

                    rawDataSheet.addCell(new Number(1, (j + 5), numOfLabs, centerFormat));
                    j++;
                }

            }
            j = 1;
            // counts how many labs have each question (Reports Returned column)
            String numOfReturnedQuery = "SELECT questions.questionId, count(labdetails.labId), categoryId FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND year(inspectionDate) = " + year + " group by questionId";
            ps = con.prepareStatement(numOfReturnedQuery);
            returnedRS = ps.executeQuery();

            int labsReturned;
            currentCategoryId = firstCategoryId;
            while (returnedRS.next()) {
                lastQuestionId = currentCategoryId;
                currentCategoryId = returnedRS.getInt(3);

                if (lastQuestionId == currentCategoryId) {
                    labsReturned = returnedRS.getInt(2); // number of labs that are Done and contain the current question (questionId)

                    rawDataSheet.addCell(new Number(2, (j + 5), labsReturned, centerFormat));
                    j++;
                } else {
                    j += 2;
                    labsReturned = returnedRS.getInt(2); // number of labs that are Done and contain the current question (questionId)

                    rawDataSheet.addCell(new Number(2, (j + 5), labsReturned, centerFormat));
                    j++;
                }

            }
            j = 1;

            //This is for Non Compliant Incident Sheet
            //returns the number of labs that are DONE
            String query = "SELECT questions.questionId, count(labdetails.labId), categoryId, questions.question FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND year(inspectionDate) = " + year + " group by questionId";
            ps = con.prepareStatement(query);
            nonCompRS = ps.executeQuery();

            //these 2 are combined so I can calculate the compliance rate and na returned
            //number of non compliant labs for each question
            String numOfNonCompliantQuery = "SELECT questions.questionId, count(labdetails.labId), categoryId, questions.question FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND comments != '' AND year(inspectionDate) = " + year + " group by questionId";
            ps = con.prepareStatement(numOfNonCompliantQuery);
            nonCompliantRS = ps.executeQuery();
            int nonCompliantReturned;

            //number of compliant labs for each question
            String numOfCompliantQuery = "SELECT questions.questionId, count(labdetails.labId), categoryId, questions.question FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND comments = '' AND year(inspectionDate) = " + year + " group by questionId";
            ps = con.prepareStatement(numOfCompliantQuery);
            compliantRS = ps.executeQuery();
            int compliantReturned;

            currentCategoryId = firstCategoryId;
            int lastQuestionIdCompliant;
            int currentQuestionIdCompliant = firstCategoryId;

            int[] countArr = new int[2];
            countArr[0] = 0;
            countArr[1] = 5;

            while (nonCompliantRS.next() && compliantRS.next() && nonCompRS.next()) {

                String question = nonCompliantRS.getString(4);
                lastQuestionId = currentCategoryId;
                lastQuestionIdCompliant = currentQuestionIdCompliant;
                currentCategoryId = nonCompliantRS.getInt(3);
                currentQuestionIdCompliant = compliantRS.getInt(3);

                if (lastQuestionId == currentCategoryId && lastQuestionIdCompliant == currentQuestionIdCompliant) {

                    nonCompliantReturned = nonCompliantRS.getInt(2); // number of labs that have returned with i question as No
                    compliantReturned = compliantRS.getInt(2); // number of labs that have returned with i question as Yes

                    //if nonCompliantRetruned is not 0 then it needs to be added to the Non-Compliant sheet as well
                    //I do this here so I don't need another while loop for Non-Compliant returned this is quicker
                    if (nonCompliantReturned != 0) {
                        countArr = nonCompliantSheetCreation(nonCompliantSheet, currentCategoryId, question, nonCompRS.getInt(2), nonCompliantReturned, compliantReturned, countArr, categoryList);
                    }

                    rawDataSheet.addCell(new Number(4, (j + 5), nonCompliantReturned, centerFormat));

                    rawDataSheet.addCell(new Number(3, (j + 5), compliantReturned, centerFormat));

                    int naReturned = compliantReturned - nonCompliantReturned;
                    if (naReturned < 0) {
                        naReturned = 0;
                    }

                    rawDataSheet.addCell(new Number(5, (j + 5), naReturned, centerFormat));

                    int complianceRate;
                    try {
                        complianceRate = (int) ((double) (((compliantReturned - nonCompliantReturned) / (double) compliantReturned) * 100));

                        rawDataSheet.addCell(new Number(6, (j + 5), complianceRate, centerFormat));
                    } catch (ArithmeticException e) {

                        rawDataSheet.addCell(new Number(6, (j + 5), 0, centerFormat));
                    }
                    j++;
                } else {
                    j += 2;

                    nonCompliantReturned = nonCompliantRS.getInt(2); // number of labs that have returned with i question as No
                    compliantReturned = compliantRS.getInt(2); // number of labs that have returned with i question as Yes

                    if (nonCompliantReturned != 0) {
                        countArr = nonCompliantSheetCreation(nonCompliantSheet, currentCategoryId, question, nonCompRS.getInt(2), nonCompliantReturned, compliantReturned, countArr, categoryList);
                    }

                    rawDataSheet.addCell(new Number(4, (j + 5), nonCompliantReturned, centerFormat));

                    rawDataSheet.addCell(new Number(3, (j + 5), compliantReturned, centerFormat));

                    int naReturned = compliantReturned - nonCompliantReturned;
                    if (naReturned < 0) {
                        naReturned = 0;
                    }

                    rawDataSheet.addCell(new Number(5, (j + 5), naReturned, centerFormat));

                    int complianceRate;
                    try {
                        complianceRate = (int) ((double) (((compliantReturned - nonCompliantReturned) / (double) compliantReturned) * 100));

                        rawDataSheet.addCell(new Number(6, (j + 5), complianceRate, centerFormat));
                    } catch (ArithmeticException e) {

                        rawDataSheet.addCell(new Number(6, (j + 5), 0, centerFormat));
                    }

                    j++;
                }

            }

            workbook = summaryCreation(workbook, year, categoryList);
        } catch (SQLException e) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (questionNameRS != null) {
                    questionNameRS.close();
                }
                if (totalLabsRS != null) {
                    totalLabsRS.close();
                }
                if (totalLabsNotDONERS != null) {
                    totalLabsNotDONERS.close();
                }
                if (questionRS != null) {
                    questionRS.close();
                }
                if (returnedRS != null) {
                    returnedRS.close();
                }
                if (nonCompRS != null) {
                    nonCompRS.close();
                }
                if (nonCompliantRS != null) {
                    nonCompliantRS.close();
                }
                if (compliantRS != null) {
                    compliantRS.close();
                }
                if (categoryRS != null) {
                    categoryRS.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        rawDataSheet = sheetAutoFitColumns(rawDataSheet);
        nonCompliantSheet = sheetAutoFitColumns(nonCompliantSheet);
        summarySheet = sheetAutoFitColumns(summarySheet);
        compliantSummarySheet = sheetAutoFitColumns(compliantSummarySheet);
        return workbook;
    }

    private static int[] nonCompliantSheetCreation(WritableSheet nonCompliantSheets, int currentCategoryId, String question, int nonComp, int nonCompliantReturned, int compliantReturned, int[] countArr, HashMap<Integer, String> categoryList) throws WriteException, SQLException {
        // nonCompRS.next();
        //creates format for bolded titles
        WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 10);
        titleFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat titleFormat = new WritableCellFormat(titleFont);

        //keeps track of where cells need to be placed for the non-compliant incidents sheet
        int lastUsedCatId = countArr[0];
        //used int non compliant incident summary for keeping track of used category ids
        int nonCompMarker = countArr[1];

        //this is where items will be added to the noncompliant incidents sheet
        if (lastUsedCatId != currentCategoryId || lastUsedCatId == 0) {
            nonCompMarker += 1;

            //switch that adds headers when they are needed
            nonCompliantSheets.addCell(new Label(0, nonCompMarker, categoryList.get(currentCategoryId), titleFormat));
            nonCompMarker++;
            categoryList.remove(0);

        }
        lastUsedCatId = currentCategoryId; //makes lastUsedCatId the current ID in case there are multiple questions that have non compliant incidents

        nonCompliantSheets.addCell(new Label(0, nonCompMarker, question));
        nonCompliantSheets.addCell(new Number(1, nonCompMarker, nonComp));
        nonCompliantSheets.addCell(new Number(2, nonCompMarker, compliantReturned));
        nonCompliantSheets.addCell(new Number(3, nonCompMarker, nonCompliantReturned));

        nonCompMarker++;

        countArr[0] = lastUsedCatId;
        countArr[1] = nonCompMarker;

        return countArr;
    }

    private static WritableWorkbook summaryCreation(WritableWorkbook workbook, int year, HashMap<Integer, String> categoryList) throws ClassNotFoundException, WriteException {

        WritableSheet summarySheet = workbook.getSheet("Summary");
        WritableSheet compliantSummarySheet = workbook.getSheet("Compliant Summary");

        NumberFormat percent = new NumberFormat("#%");
        WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10);
        boldFont.setBoldStyle(WritableFont.BOLD);
        WritableCellFormat percentFormat = new WritableCellFormat(percent);
        percentFormat.setFont(boldFont);

        //totalReturnedQuery is the only query that will contain the question category information
        String totalReturnedQuery = "SELECT count(labdetails.labId), questions.categoryId FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND year(inspectionDate) = " + year + " group by questions.categoryId";
        String compliantReturnedQuery = "SELECT count(labdetails.labId) FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND comments = '' AND year(inspectionDate) = " + year + " group by questions.categoryId";
        String nonCompliantReturnedQuery = "SELECT count(labdetails.labId) FROM responses RIGHT JOIN questions ON questions.questionId = responses.questionId LEFT JOIN labdetails ON labdetails.labId = responses.labId AND status = 'Done' AND comments != '' AND year(inspectionDate) = " + year + " group by questions.categoryId";

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet totalReturnedRS = null;
        ResultSet compliantReturnedRS = null;
        ResultSet nonCompliantReturnedRS = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(totalReturnedQuery);
            totalReturnedRS = ps.executeQuery();

            ps = con.prepareStatement(compliantReturnedQuery);
            compliantReturnedRS = ps.executeQuery();

            ps = con.prepareStatement(nonCompliantReturnedQuery);
            nonCompliantReturnedRS = ps.executeQuery();

            int cellNumber = 5; // starting cell location for summary
            int cellNumber2 = 3; // staring cell location for compliant summary

            while (totalReturnedRS.next() && compliantReturnedRS.next() && nonCompliantReturnedRS.next()) {

                int totalReturned = totalReturnedRS.getInt(1); // gets total returned column
                int compliantReturned = compliantReturnedRS.getInt(1); // gets compliant of total returned column
                int nonCompliantReturned = nonCompliantReturnedRS.getInt(1); // gets non-compliant of total returned column

                int nonComplianceRate = 0;

                if (nonCompliantReturned > 0) {

                    nonComplianceRate = Math.round((nonCompliantReturned / (float) totalReturned) * 100); // non-compliance rate column
                }
                float complianceRate = 100 - nonComplianceRate; // compliance rate column

                int questionCategoryId = totalReturnedRS.getInt(2); // gets question category Id

                summarySheet.addCell(new Label(0, cellNumber, categoryList.get(questionCategoryId)));
                compliantSummarySheet.addCell(new Label(0, cellNumber2, categoryList.get(questionCategoryId)));
                categoryList.remove(0);

                summarySheet.addCell(new Number(1, cellNumber, totalReturned));
                summarySheet.addCell(new Number(2, cellNumber, compliantReturned));
                summarySheet.addCell(new Number(3, cellNumber, nonCompliantReturned));
                summarySheet.addCell(new Number(4, cellNumber, nonComplianceRate));
                summarySheet.addCell(new Number(5, cellNumber, complianceRate));
                compliantSummarySheet.addCell(new Number(1, cellNumber2, complianceRate / 100, percentFormat));
                cellNumber++;
                cellNumber2++;

            }
        } catch (SQLException e) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (totalReturnedRS != null) {
                    totalReturnedRS.close();
                }
                if (compliantReturnedRS != null) {
                    compliantReturnedRS.close();
                }
                if (nonCompliantReturnedRS != null) {
                    nonCompliantReturnedRS.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return workbook;
    }

    private static WritableWorkbook headerCreation(WritableWorkbook workbook, WritableCellFormat titleFormat, int year) throws WriteException {

        //The following is for the Raw Data sheet
        WritableSheet rawDataSheet = workbook.getSheet("Raw Data");

        rawDataSheet.addCell(new Label(0, 0, "Laboratory Inspection Raw Data Report " + year, titleFormat));

        rawDataSheet.addCell(new Label(1, 2, "Labs Inspected", titleFormat));

        rawDataSheet.addCell(new Label(2, 2, "Reports Returned", titleFormat));

        rawDataSheet.addCell(new Label(3, 2, "Compliant", titleFormat));

        rawDataSheet.addCell(new Label(4, 2, "Non-Compliant", titleFormat));

        rawDataSheet.addCell(new Label(5, 2, "N/A", titleFormat));

        rawDataSheet.addCell(new Label(6, 2, "Compliance Rate (% of returned)", titleFormat));

        //the following is for the Non-Compliance Incidents sheet
        WritableSheet nonComplianceSheet = workbook.getSheet("Non-Compliant Incidents");

        nonComplianceSheet.addCell(new Label(0, 0, "Laboratory Inspection Non-Compliance Invidents Report", titleFormat));

        nonComplianceSheet.addCell(new Label(1, 2, "Reports Returned", titleFormat));

        nonComplianceSheet.addCell(new Label(2, 2, "Compliant", titleFormat));

        nonComplianceSheet.addCell(new Label(3, 2, "Non-Compliant", titleFormat));

        //the following is for the Summary sheet
        WritableSheet summarySheet = workbook.getSheet("Summary");

        summarySheet.addCell(new Label(0, 0, "Laboratory Inspection Summary Report " + year + " - Totals are for specified criteria", titleFormat));

        summarySheet.addCell(new Label(1, 2, "Total Returned"));

        summarySheet.addCell(new Label(2, 2, "Compliant of Total Returned"));

        summarySheet.addCell(new Label(3, 2, "Non-Compliant of Total Returned"));

        summarySheet.addCell(new Label(4, 2, "Non-Compliant Rate (% of Returned)"));

        summarySheet.addCell(new Label(5, 2, "Compliant Rate (%)"));

        //the following is for the Compliant Summary Sheet
        WritableSheet compliantSummarySheet = workbook.getSheet("Compliant Summary");

        compliantSummarySheet.addCell(new Label(0, 0, "Laboratory Inspection Summary Report " + year, titleFormat));

        compliantSummarySheet.addCell(new Label(1, 0, "Areas Inspected", titleFormat));

        compliantSummarySheet.addCell(new Label(1, 1, "Compliant Rate (% of Returned)"));

        return workbook;
    }

// This is a method that I took that was made by Neuro_sys on Stackoverflow
// It automatically fits the cells size to the largest item in the cells row
    private static WritableSheet sheetAutoFitColumns(WritableSheet sheet) {
        for (int i = 0; i < sheet.getColumns(); i++) {
            Cell[] cells = sheet.getColumn(i);
            int longestStrLen = -1;

            if (cells.length == 0) {
                continue;
            }

            /* Find the widest cell in the column. */
            for (Cell cell : cells) {
                if (cell.getContents().length() > longestStrLen) {
                    String str = cell.getContents();
                    if (str == null || str.isEmpty()) {
                        continue;
                    }
                    longestStrLen = str.trim().length();
                }
            }

            /* If not found, skip the column. */
            if (longestStrLen == -1) {
                continue;
            }

            /* If wider than the max width, crop width */
            if (longestStrLen > 70) {
                longestStrLen = 70;
            }

            CellView cv = sheet.getColumnView(i);
            cv.setSize(longestStrLen * 256 + 100); /* Every character is 256 units wide, so scale it. */

            sheet.setColumnView(i, cv);
        }
        return sheet;
    }

    public ArrayList<Integer> getLabIdsFromUserName(String username) {
        ArrayList<Integer> labIdslist = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection con = null;

        try {
            con = DatabaseConnection.getConnection();
            String query = "select labId from labdetails where responsibleIndividual = '" + username + "'";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                labIdslist.add(rs.getInt(1));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        return labIdslist;
    }

    public ArrayList<Integer> getLabIdsFromFullNameUserView(String fullName) {
        ArrayList<Integer> labIdslist = new ArrayList<>();
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            String query = "select labId from labdetails where responsibleIndividual = '" + fullName + "' AND (status = 'In Progress' OR status = 'Need Faculty Confirmation')";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                labIdslist.add(rs.getInt(1));

            }

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return labIdslist;
    }

    public ArrayList<Integer> getLabIdsFromUserNameUserView(String username) {
        String query1 = "select firstName, lastName from users where northwestId = '" + username + "'";
        ArrayList<Integer> labIdslist = new ArrayList<>();
        ResultSet rs = null;
        ResultSet rs1 = null;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query1);
            rs1 = ps.executeQuery();
            String firstName = "";
            String lastName = "";
            while (rs1.next()) {
                firstName = rs1.getString("firstName");
                lastName = rs1.getString("lastName");
            }

            String fullName = lastName + ", " + firstName;
            String query = "select labId from labdetails where responsibleIndividual = '" + fullName + "' AND status = 'In Progress'";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                labIdslist.add(rs.getInt(1));

            }

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (rs1 != null) {
                    rs1.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return labIdslist;
    }

    public static List<Integer> getQuestionIdsFromLabId(int labId) throws ClassNotFoundException {
        String query = "SELECT questionId from responses where labId = " + labId;
        ResultSet questionIdRS = null;
        List<Integer> questionIdsList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            questionIdRS = ps.executeQuery();

            while (questionIdRS.next()) {
                questionIdsList.add(questionIdRS.getInt(1));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (questionIdRS != null) {
                    questionIdRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return questionIdsList;
    }

    public static void updateQuestionCompliance(int labId, ArrayList<Integer> compliantQuestionIds) throws ClassNotFoundException {

        String query = "UPDATE responses \n set compliant = CASE ";
        String responses = "SELECT questionId FROM responses WHERE labId = " + labId + " AND compliant = 'No'"; //gets questionId for the current lab
        ResultSet questionIdRS = null;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(responses);
            questionIdRS = ps.executeQuery();

            String compliantIdsUsed = "WHERE questionId in (";

            int counter = 0;
            int firstIf = 0;
            while (questionIdRS.next()) {
                try {
                    if (questionIdRS.getInt(1) == compliantQuestionIds.get(counter)) {

                        query = query.concat("when questionId = " + compliantQuestionIds.get(counter) + " THEN 'Yes' \n");
                        if (firstIf == 0) {
                            compliantIdsUsed = compliantIdsUsed.concat(compliantQuestionIds.get(counter) + "");
                        } else {
                            compliantIdsUsed = compliantIdsUsed.concat(", " + compliantQuestionIds.get(counter));
                        }
                        counter++;
                        firstIf = 1; //just used to see when the first time the program enters this If statement
                    }
                } catch (IndexOutOfBoundsException e) {

                }

            }
            compliantIdsUsed = compliantIdsUsed.concat(")");
            query = query.concat("END \n " + compliantIdsUsed);

            ps = con.prepareStatement(query);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (questionIdRS != null) {
                    questionIdRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void updateCommentsCompliance(String[] adminCommentsArray, int labId, int commentSelect) throws ClassNotFoundException {
        ResultSet responsesRS = null;
        PreparedStatement ps = null;
        Connection con = null;
        try {
            String query;
            if (commentSelect == 1) {

                query = "UPDATE responses set adminComments = ";//first part of the query should not change unless the table or the column changes
            } else {
                query = "UPDATE responses set comments = ";//first part of the query should not change unless the table or the column changes
            }
            String responses = "SELECT responseId FROM responses WHERE labId = " + labId; //gets responseId for the current lab
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(responses);
            responsesRS = ps.executeQuery();

            String adminComments = "case when responseId = ";
            String compliance = "compliant = case when responseId = ";
            String responseArray = "(";

            int counter = 0;
            while (responsesRS.next()) {

                if (counter != 0) {
                    adminComments = adminComments.concat(" when responseId = ");
                    compliance = compliance.concat(" when responseId = ");
                }

                adminComments = adminComments.concat(responsesRS.getInt(1) + " then '" + adminCommentsArray[counter] + "'");

                compliance = compliance.concat(responsesRS.getInt(1) + " then 'No'");

                if (responsesRS.isLast()) {
                    responseArray = responseArray.concat(responsesRS.getInt(1) + ")");
                } else {
                    responseArray = responseArray.concat(responsesRS.getInt(1) + ", ");
                }
                counter++;
            }

            adminComments = adminComments.concat("end, ");
            compliance = compliance.concat("end ");
            query = query.concat(adminComments + compliance + "where responseId in " + responseArray);

            ps = con.prepareStatement(query);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (responsesRS != null) {
                    responsesRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static int isAdmin(String username) throws ClassNotFoundException {
        String query = "SELECT role FROM users WHERE northwestId = '" + username + "'";
        ResultSet isAdminRS = null;
        Connection con = null;
        PreparedStatement ps = null;
        int role = 0;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            isAdminRS = ps.executeQuery();

            while (isAdminRS.next()) {
                role = isAdminRS.getInt(1);

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (isAdminRS != null) {
                    isAdminRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return role;
    }

    public void changeLabStatus(int labId, String status) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;

        String query = "UPDATE labdetails SET status = '" + status + "' WHERE labId = " + labId;

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                con.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public boolean checkResponses(int labId) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "select exists (select * from responses where labId = " + labId + ")";

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            rs.next();
            return rs.getInt(1) == 1;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                con.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return false;

    }

    public void sendMail(String to, int mailType, String html) {

        String from = "citedev@nwmissouri.edu";
        String host = "smtp.office365.com";
        String port = "587";
        Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.port", port);
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("citedev@nwmissouri.edu", "DevLop_sp17");
            }
        });

        try {

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            if (mailType == 1) {
                // Set Subject: header field
                message.setSubject("New Lab inspected");
            } else if (mailType == 2) {
                message.setSubject("Lab has been reviewed");
            } else {
                message.setSubject("Lab has been completed");
            }

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Now set the actual message
            message.setText(html, "UTF-8", "html");

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
        }

    }

    public String[] getLabBuildingRoomAssignmentPerson(int labId) {
        String query = "SELECT building, roomNumber, roomAssignment, responsibleIndividual FROM labdetails WHERE labId = " + labId;

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String[] labLocation = new String[4];

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            rs.next();
            labLocation[0] = rs.getString(1); //building
            labLocation[1] = rs.getString(2); //roomNumber
            labLocation[2] = rs.getString(3); //roomAssignment
            labLocation[3] = rs.getString(4); //responsibleIndividual

            return labLocation;

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return labLocation;
    }

    public String getEmailFromLabId(int labId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String getName = "SELECT responsibleIndividual FROM labdetails WHERE labId = " + labId;
        String name;
        String id = "";
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(getName);
            rs = ps.executeQuery();
            rs.next();
            name = rs.getString(1);

            String[] nameArray = name.split(", ");
            String lastName = nameArray[0];
            String firstName = nameArray[1];

            String getUserId = "SELECT northwestId FROM users WHERE firstName = '" + firstName + "' AND lastName = '" + lastName + "'";

            ps = con.prepareStatement(getUserId);
            rs = ps.executeQuery();
            rs.next();
            id = rs.getString(1);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return id + "@nwmissouri.edu";

    }

    public static ArrayList<QuestionandCategories> getQuestionswithCategories() throws ClassNotFoundException {
        ArrayList<QuestionandCategories> qc = new ArrayList<>();
        String query = "SELECT questionID,categoriesType,question FROM questions,categories where questions.categoryId = categories.categoriesId ORDER BY categoriesId";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet questionsCategoriesRS = null;

        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            questionsCategoriesRS = ps.executeQuery();

            while (questionsCategoriesRS.next()) {
                qc.add(new QuestionandCategories(questionsCategoriesRS.getInt(1), questionsCategoriesRS.getString(2), questionsCategoriesRS.getString(3)));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (questionsCategoriesRS != null) {
                    questionsCategoriesRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return qc;
    }

    public static List userViewTable(int labId) throws ClassNotFoundException {

        String query = "select question, questions.questionId, compliant, comments, facultyComments, adminComments from responses INNER join questions on responses.questionId = questions.questionId where labId = " + labId;
        ResultSet labTableRS = null;
        List<EditLabTable> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            labTableRS = ps.executeQuery();

            while (labTableRS.next()) {

                list.add(new EditLabTable(labTableRS.getString(1), labTableRS.getInt(2), labTableRS.getString(3), labTableRS.getString(4), labTableRS.getString(5), labTableRS.getString(6)));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (labTableRS != null) {
                    labTableRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return list;
    }

    public static List editLabTable(int labId) throws ClassNotFoundException {

        String query = "select question, questions.questionId, compliant, comments, facultyComments, adminComments from responses INNER join questions on responses.questionId = questions.questionId where labId = " + labId + "";
        ResultSet labTableRS = null;
        List<EditLabTable> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            labTableRS = ps.executeQuery();

            while (labTableRS.next()) {
                list.add(new EditLabTable(labTableRS.getString(1), labTableRS.getInt(2), labTableRS.getString(3), labTableRS.getString(4), labTableRS.getString(5), labTableRS.getString(6)));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataCRUD.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (labTableRS != null) {
                    labTableRS.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DataCRUD.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        return list;
    }
}
