/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author s515739
 */
@WebServlet(name = "AdminUserSwitchServlet", urlPatterns = {"/AdminUserSwitchServlet"})
public class AdminUserSwitchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RequestDispatcher rd;

        String fromAdminView = request.getParameter("fromAdminView");
        String username = request.getParameter("userName");
        String fullName = request.getParameter("fullName");

        if (fromAdminView.equals("1")) {
            String fullNameFormatted = fullNameFormat(fullName);
            request.setAttribute("AdminViewSwitch", "true");
            DataCRUD dc = new DataCRUD();
            ArrayList<Integer> labIdsArraylist = dc.getLabIdsFromFullNameUserView(fullNameFormatted);
            request.setAttribute("userName", username);
            ArrayList<LabAudit> labAuditsArraylist = new ArrayList<>();
            for (int i = 0; i < labIdsArraylist.size(); i++) {
                labAuditsArraylist.add(dc.getLabDetailsFromLabIdUserView(labIdsArraylist.get(i)));
            }
            request.setAttribute("LabAuditsArray", labAuditsArraylist);
            request.setAttribute("fullName", fullName);
            rd = request.getRequestDispatcher("UserLabs.jsp");

        } else {
            rd = request.getRequestDispatcher("AdminView.jsp");
            request.setAttribute("UserViewSwitch", "true");
            request.setAttribute("userName", username);
        }

        rd.forward(request, response);

    }

    public static String fullNameFormat(String fullName) {

        if (!fullName.contains(",")) {
            String[] fullNameSplit = fullName.split(" ");
            String firstName = fullNameSplit[0];
            String lastName = fullNameSplit[1];

            String fullNameFormatted = lastName + ", " + firstName;
            return fullNameFormatted;
        }
        return fullName;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
