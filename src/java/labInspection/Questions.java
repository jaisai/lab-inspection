package labInspection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author S525096
 */
public final class Questions {

    private int questionId;
    private String question;
    private int categoryId;
    private final String categoryName;

    public Questions(int questionId, String question, int categoryId) {
        this.questionId = questionId;
        this.question = question;
        this.categoryId = categoryId;
        this.categoryName = setCategoryName(categoryId);
    }

    public String setCategoryName(int categoryId) {

        String query = "SELECT categoriesType FROM categories where categoriesId = " + categoryId;

        ResultSet catNameRS = null;
        Connection con = null;
        PreparedStatement ps = null;
        String catName = "";
        try {
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            catNameRS = ps.executeQuery();

            catNameRS.next();

            catName = catNameRS.getString(1);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (catNameRS != null) {
                    catNameRS.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return catName;

    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}
