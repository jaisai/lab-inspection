/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author S525096
 */
@WebServlet(name = "UpdateLabInfoServlet", urlPatterns = {"/UpdateLabInfoServlet"})
public class UpdateLabInfoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        SimpleDateFormat SDF = new SimpleDateFormat(" EEE MMM dd yyyy");
        String sendToFaculty = request.getParameter("SendToFaculty");//this will not be null if the admin wants to send this lab to be reviewed by faculty otherwise it will be null
        String needConfirmation = request.getParameter("NeedConfirmation"); // this will not be null if the admin wants to save the results but not send it to faculty otherwise it will be null
        //gets lab data
        int labid = Integer.parseInt(request.getParameter("labId"));
        String building = request.getParameter("building");
        String roomNo = request.getParameter("roomNo");
        String discipline = request.getParameter("discipline");
        String responsibleIndividual = request.getParameter("responsibleIndividual");
        String roomAssignemt = request.getParameter("roomAssignemt");
        HttpSession sess = request.getSession(false);
        String status = request.getParameter("Status");

        List<Integer> questionIdList = DataCRUD.getQuestionIdsFromLabId(labid);
        ArrayList<Integer> compliantQuestionId = new ArrayList<>();

        int notCompliant = 0; //this will be 1 if there is a non compliant question
        for (Integer questionIdList1 : questionIdList) {
            String checkBox = request.getParameter(questionIdList1 + "");
            if (checkBox != null) {
                if (checkBox.toLowerCase().equals("on")) {
                    compliantQuestionId.add(questionIdList1);
                }
            } else {
                notCompliant = 1;
            }
        }

        PreparedStatement ps;
        Connection con;

        if (sendToFaculty != null) {

            if (notCompliant == 1) {

                String query = "UPDATE labdetails set status = 'In Progress' where labId = " + labid;
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
                con.close();

            } else {
                String query = "UPDATE labdetails set status = 'Need Faculty Confirmation' where labId = " + labid;
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
                con.close();
            }
            DataCRUD dc = new DataCRUD();
            String email = dc.getEmailFromLabId(labid);
            String[] labLocation = dc.getLabBuildingRoomAssignmentPerson(labid);
            String message = "<img src='http://www.nwmissouri.edu/marketing/images/design/logos/N60-2Stack-G.png' width='128' height='128' alt='Northwest Logo'>"
                    + "<p>The " + labLocation[2] + " in " + labLocation[0] + ", room " + labLocation[1] + ", has been inspected. Please follow the link to view your lab's inspection.</p>"
                    + "<br />"
                    + "<a href='http://cite7.nwmissouri.edu:3000/LabInspection'>LabInspection</a>";

            dc.sendMail(email, 2, message);
        } else {
            if (!status.equals("Waiting to Send")) {
                String query = "UPDATE labdetails set status = 'Need Faculty Confirmation' where labId = " + labid;
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
                con.close();
            } else {
                String query = "UPDATE labdetails set status = 'Waiting to Send' where labId = " + labid;
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
                con.close();
            }

        }
        String[] adminComments = request.getParameterValues("adminComments");
        String[] adminCommentsOrderString = request.getParameterValues("adminCommentsOrder");
        String[] adminCommentsNew = request.getParameterValues("adminCommentsNew");
        String[] originalComments = request.getParameterValues("originalComments");
        String[] originalCommentsOrderString = request.getParameterValues("originalCommentsOrder");
        String[] originalCommentsNew = request.getParameterValues("originalCommentsNew");
        String[] originalCommentsNewOrderString = request.getParameterValues("originalCommentsNewOrder");

        int skipOCN = 0;

        int[] adminCommentsOrder = new int[adminCommentsOrderString.length];
        int[] originalCommentsOrder = new int[originalCommentsOrderString.length];
        int[] originalCommentsNewOrder = null;
        try {
            originalCommentsNewOrder = new int[originalCommentsNewOrderString.length];
        } catch (NullPointerException e) {
            skipOCN = 1;
        }

        for (int i = 0; i < adminCommentsOrderString.length; i++) {
            adminCommentsOrder[i] = Integer.parseInt(adminCommentsOrderString[i]);
            originalCommentsOrder[i] = Integer.parseInt(originalCommentsOrderString[i]);
            if (skipOCN == 0) {
                originalCommentsNewOrder[i] = Integer.parseInt(originalCommentsNewOrderString[i]);
            }
        }

        //checks if either admin or original comments inputs are null and only updates the ones that are not null
        if (!status.equals("Waiting to Send")) {
            if (adminComments != null && adminCommentsNew != null) {
                for (int i = 0; i < adminComments.length; i++) {
                    if (adminComments[i].equals("") || adminCommentsNew[i].equals("")) {
                        adminComments[i] = adminComments[i].concat(adminCommentsNew[i]);
                    } else {
                        adminComments[i] = adminComments[i].concat("; " + adminCommentsNew[i]);
                    }
                }
                adminComments = sortList(adminComments, adminCommentsOrder);
                DataCRUD.updateCommentsCompliance(adminComments, labid, 1);
            }
        } else {
            try {
                for (int i = 0; i < originalComments.length; i++) {
                    if (originalComments[i].equals("") || originalCommentsNew[i].equals("")) {
                        originalComments[i] = originalComments[i].concat(originalCommentsNew[i]);
                    } else {
                        originalComments[i] = originalComments[i].concat("; " + originalCommentsNew[i]);
                    }
                }
                originalComments = sortList(originalComments, originalCommentsOrder);
                DataCRUD.updateCommentsCompliance(originalComments, labid, 0); //updates the comments, the 0 means the comments are original not admin (well really it means they are not admin)
            } catch (NullPointerException e) {
                originalCommentsNew = sortList(originalCommentsNew, originalCommentsNewOrder);
                DataCRUD.updateCommentsCompliance(originalCommentsNew, labid, 0); //updates the comments, the 0 means the comments are original not admin (well really it means they are not admin)
            }
        }

        if (!compliantQuestionId.isEmpty()) {
            DataCRUD.updateQuestionCompliance(labid, compliantQuestionId);
        }

        request.setAttribute(
                "UserViewSwitch", sess.getAttribute("UserViewSwitch"));
        request.getSession()
                .setAttribute("inspectionLabs", DataCRUD.getLabDetails());
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/AdminView.jsp");

        requestDispatcher.forward(request, response);


    }

    public String[] sortList(String[] comments, int[] order) {

        ArrayList<Pair> list = new ArrayList<>();

        for (int i = 0; i < comments.length; i++) {
            list.add(new Pair(comments[i], order[i]));
        }

        Collections.sort(list, new PairComparator());

        for (int i = 0; i < comments.length; i++) {
            comments[i] = list.get(i).getComment();
        }

        return comments;
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UpdateLabInfoServlet.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UpdateLabInfoServlet.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class Pair {

    String comments;
    int order;

    public Pair(String commentsIn, int orderIn) {
        comments = commentsIn;
        order = orderIn;
    }

    public int getOrder() {
        return order;
    }

    public String getComment() {
        return comments;
    }

}

class PairComparator implements Comparator<Pair> {

    @Override
    public int compare(Pair o1, Pair o2) {
        if (o1.getOrder() > o2.getOrder()) {
            return 1;
        } else if (o1.getOrder() < o2.getOrder()) {
            return -1;
        } else {
            return 0;
        }
    }

}
