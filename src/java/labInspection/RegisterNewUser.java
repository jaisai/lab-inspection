/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

/**
 *
 * @author S525096
 */
public class RegisterNewUser {
    private String northwestId;
    private String firstName;
    private String lastName;
    private String password;
    private int role;

    public RegisterNewUser(String northwestId, String firstName, String lastName, String password, int role) {
        this.northwestId = northwestId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.role = role;
    }

    public String getNorthwestId() {
        return northwestId;
    }

    public void setNorthwestId(String northwestId) {
        this.northwestId = northwestId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RegisterNewUser{" + "northwestId=" + northwestId + ", firstName=" + firstName + ", lastName=" + lastName + ", password=" + password + ", role=" + role + '}';
    }   
    
}
