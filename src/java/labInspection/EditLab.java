/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S525096
 */
@WebServlet(name = "EditLab", urlPatterns = {"/EditLab"})
public class EditLab extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int labId = Integer.parseInt(request.getParameter("EditLabId"));

        LabAudit labAudit = DataCRUD.getLabDetailsFromLabId(labId);
        String building = labAudit.getBuilding();
        String discipline = labAudit.getDiscipline();
        String responsibleIndividualName = labAudit.getResponsibleIndividualName();
        String status = labAudit.getStatus();
        String inspectionDate = labAudit.getInspectionDate();
        String roomAssignment = labAudit.getRoomAssignment();
        String roomNumber = labAudit.getRoomNumber();
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println("LabId" + labId);
            out.println("Building" + building);
            out.println("LabAudit" + labAudit.getLabId());
            request.setAttribute("EditLabId", labId);
            request.setAttribute("Building", building);
            request.setAttribute("Discipline", discipline);
            request.setAttribute("ResponsibleIndividualName", responsibleIndividualName);
            request.setAttribute("Status", status);
            request.setAttribute("InspectionDate", inspectionDate);
            request.setAttribute("RoomAssignment", roomAssignment);
            request.setAttribute("RoomNumber", roomNumber);

            RequestDispatcher rd = request.getRequestDispatcher("EditLab.jsp");
            rd.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
