/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author S525096
 */
@WebServlet(name = "ValidateServlet", urlPatterns = {"/ValidateServlet"})
public class ValidateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        RequestDispatcher rd;

        String url = "ldap://proxis.nwmissouri.edu:389";
        String[] attrIdsToSearch = new String[]{"memberOf", "givenName", "SN"};
        String SEARCH_BY_SAM_ACCOUNT_NAME = "(sAMAccountName=%s)";
        String userBase = "DC=nwmissouri,DC=edu";

        Hashtable<String, String> environment = new Hashtable<>();

        environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, url);
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, username + "@nwmissouri.edu");
        environment.put(Context.SECURITY_CREDENTIALS, password);
        try {
            InitialDirContext context = new InitialDirContext(environment);

            String filter = String.format(SEARCH_BY_SAM_ACCOUNT_NAME, username);
            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
            constraints.setReturningAttributes(attrIdsToSearch);
            NamingEnumeration results = context.search(userBase, filter, constraints);

            // Fail if no entries found
            if (results == null || !results.hasMore()) {
                System.out.println("No result found");
                return;
            }

            // Get result for the first entry found
            SearchResult result = (SearchResult) results.next();
            //parse through string and retrieve first and last name
            String resultStr = result.toString();
            String firstName = resultStr.substring(resultStr.indexOf("Name: ") + 6, resultStr.indexOf(", m"));
            String lastName = resultStr.substring(resultStr.indexOf("sn=sn: ") + 7, resultStr.indexOf("}"));
            String fullName = firstName + " " + lastName;
            request.setAttribute("fullName", fullName);
            //  SearchResult result = (SearchResult) results.next();
            NameParser parser = context.getNameParser("");
            Name entryName = parser.parse(new CompositeName(result.getName())
                    .get(0));
            int adminLvl = DataCRUD.isAdmin(username);
            //if the user is staff then admin. For testing change it to Student Users
            if (adminLvl == 3) {
                rd = request.getRequestDispatcher("AdminView.jsp");
                request.setAttribute("UserViewSwitch", "true");
                request.setAttribute("userName", username);
            } else if (adminLvl == 1) {
                rd = request.getRequestDispatcher("AdminView.jsp");
                request.setAttribute("UserViewSwitch", "false");
                request.setAttribute("userName", username);
            } else {
                request.setAttribute("AdminViewSwitch", "false");
                DataCRUD dc = new DataCRUD();
                ArrayList<Integer> labIdsArraylist = dc.getLabIdsFromUserNameUserView(username);
                request.setAttribute("userName", username);
                ArrayList<LabAudit> labAuditsArraylist = new ArrayList<>();
                for (Integer labIdsArraylist1 : labIdsArraylist) {
                    labAuditsArraylist.add(DataCRUD.getLabDetailsFromLabIdUserView(labIdsArraylist1));
                }
                request.setAttribute("LabAuditsArray", labAuditsArraylist);
                rd = request.getRequestDispatcher("UserLabs.jsp");
            }
            HttpSession httpSession = request.getSession();
            //httpSession.setAttribute("user", );
            rd.forward(request, response);
        } catch (NamingException | ClassNotFoundException | ServletException | IOException e) {
            request.setAttribute("Incorrect", "true");
            rd = request.getRequestDispatcher("Login.jsp");
            rd.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
