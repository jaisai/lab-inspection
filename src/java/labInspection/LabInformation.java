/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S525096
 */
public class LabInformation {

    private ResultSet labInfoRS;
    private final Connection con;
    private final PreparedStatement ps;
    private ArrayList<Labs> labs = new ArrayList<>();

//If you use the ResultSet LabInfoRS make sure that when you are done you set it back to the beginning
//using the .beforeFirst() method;
    public LabInformation() throws ClassNotFoundException, SQLException {
        String query = "SELECT Building, Discipline, RoomNumber, Assignment, ResponsiblePerson, labInfoId FROM labinformation";
        con = DatabaseConnection.getConnection();
        ps = con.prepareStatement(query);
        labInfoRS = ps.executeQuery();

        while (labInfoRS.next()) {

            labs.add(new Labs(labInfoRS.getInt(6), labInfoRS.getString(1), labInfoRS.getString(2), labInfoRS.getString(3), labInfoRS.getString(4), labInfoRS.getString(5)));

        }
        labInfoRS.beforeFirst();
    }

    public ResultSet getLabInfoRS() {
        return labInfoRS;
    }

    public void setLabInfoRS(ResultSet labInfoRS) {
        this.labInfoRS = labInfoRS;
    }

    public ArrayList<Labs> getLabs() {
        return labs;
    }

    public void setLabs(ArrayList<Labs> labs) {
        this.labs = labs;
    }

    public String getRoomNumber(String assignment) throws SQLException {
        labInfoRS.beforeFirst();
        while (labInfoRS.next()) {
            if (labInfoRS.getString(3).equals(assignment)) {

                return labInfoRS.getString(5);
            }
        }
        labInfoRS.beforeFirst();
        return null;
    }

    public String getResponsiblePerson(String assignment) throws SQLException {
        labInfoRS.beforeFirst();
        while (labInfoRS.next()) {
            if (labInfoRS.getString(3).equals(assignment)) {

                return labInfoRS.getString(4);
            }
        }
        labInfoRS.beforeFirst();
        return null;
    }

    public ArrayList<String> getAssignments(String building, String discipline) throws SQLException {

        ArrayList<String> assignments = new ArrayList<>();

        while (labInfoRS.next()) {
            if (labInfoRS.getString(1).equals(building) && labInfoRS.getString(2).equals(discipline)) {
                assignments.add(labInfoRS.getString(3));
            }
        }
        labInfoRS.beforeFirst();
        return assignments;
    }

    public ArrayList<String> getDiscipline(String building) throws SQLException {
        ArrayList<String> discipline = new ArrayList<>();

        while (labInfoRS.next()) {
            if (labInfoRS.getString(1).equals(building) && !discipline.contains(labInfoRS.getString(2))) {
                discipline.add(labInfoRS.getString(2));
            }
        }
        labInfoRS.beforeFirst();
        return discipline;
    }

    public ArrayList<String> getBuildings() throws SQLException {

        ArrayList<String> buildings = new ArrayList<>();

        while (labInfoRS.next()) {
            if (!buildings.contains(labInfoRS.getString(1))) {
                buildings.add(labInfoRS.getString(1));
            }
        }
        labInfoRS.beforeFirst();
        return buildings;
    }

    public void closeRS() {
        try {
            if (con != null) {
                con.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (labInfoRS != null) {
                labInfoRS.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(LabInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
