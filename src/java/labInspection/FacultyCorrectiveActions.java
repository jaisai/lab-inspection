/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author s515739
 */
@WebServlet(name = "FacultyCorrectiveActions", urlPatterns = {"/FacultyCorrectiveActions"})
public class FacultyCorrectiveActions extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

        String button = (String) request.getParameter("save");
        int sendToInspector = 0;
        if (button == null) {
            sendToInspector = 1;
        }

        int completed = Integer.parseInt(request.getParameter("Completed"));
        String viewSwitch = request.getParameter("viewSwitch");
        int skip = 0;
        //get all of the faculty comments
        String[] facultyCommentsNew = request.getParameterValues("facultyCommentsNew");
        //gets labId
        int labId = Integer.parseInt(request.getParameter("labId"));
        //gets fullName
        String fullName = (String) request.getParameter("fullName");
        //gets userName for use in the navbar
        String userName = (String) request.getParameter("userName");

        String query;
        PreparedStatement ps = null;
        ResultSet responseRS = null;
        Connection con = null;

        int allCommentsFilled = 1; //used to determine if all of the faculty comments have had input for the reviewer to review if yes then it will be 1 otherwise 0
        try {
            //checks if faculty comments are filled
            for (String facultyCommentsNew1 : facultyCommentsNew) {
                if (facultyCommentsNew1.equals("")) {
                    allCommentsFilled = 0;
                }
            }
        } catch (NullPointerException e) {
            query = "UPDATE labdetails set status = 'Done' where labId = " + labId;
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.executeUpdate();
            ps.close();
            con.close();
            DataCRUD dc = new DataCRUD();
            String email = "RKEGODE@nwmissouri.edu"; //admin email
            String[] labLocation = dc.getLabBuildingRoomAssignmentPerson(labId);
            String message = "<img src='http://www.nwmissouri.edu/marketing/images/design/logos/N60-2Stack-G.png' width='128' height='128' alt='Northwest Logo'>"
                    + "<p>The " + labLocation[2] + " located in " + labLocation[0] + ", room " + labLocation[1] + ", has been completed. Please follow this link to review it.</p>"
                    + "<br />"
                    + "<a href='http://cite7.nwmissouri.edu:3000/LabInspection'>LabInspection</a>";
            dc.sendMail(email, 3, message);
            allCommentsFilled = 0;
            skip = 1;
        }
        if (allCommentsFilled == 1 && sendToInspector == 1) {
            query = "UPDATE labdetails set status = 'Faculty Reviewed' where labId = " + labId;
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            ps.executeUpdate();
            ps.close();
            con.close();
            String email = "RKEGODE@nwmissouri.edu"; //admin email
            DataCRUD dc = new DataCRUD();
            String[] labLocation = dc.getLabBuildingRoomAssignmentPerson(labId);
            String message = "<img src='http://www.nwmissouri.edu/marketing/images/design/logos/N60-2Stack-G.png' width='128' height='128' alt='Northwest Logo'>"
                    + "<p>The " + labLocation[2] + " located in " + labLocation[0] + ", room " + labLocation[1] + ", has been revewied by "+labLocation[3]+". Please follow this link to look over it. </p>"
                    + "<br />"
                    + "<a href='http://cite7.nwmissouri.edu:3000/LabInspection'>LabInspection</a>";
            dc.sendMail(email, 2, message);
        }

        String[] facultyComments = request.getParameterValues("facultyComments");
        String[] facultyCommentsUpdate;
        int facCommentsVersion = 0; // 0 means facultyCommentsUpdate contains facultyComments 1 means it contains facultyCommentsNew
        if (facultyComments != null) {
            facultyCommentsUpdate = facultyComments;
        } else {
            facCommentsVersion = 1;
            facultyCommentsUpdate = facultyCommentsNew;
        }
        if (facCommentsVersion == 0) {
            if (facultyCommentsNew != null) {
                for (int i = 0; i < facultyCommentsNew.length; i++) {
                    if (!facultyCommentsUpdate[i].equals("") && !facultyCommentsNew[i].equals("")) {
                        facultyCommentsUpdate[i] = facultyCommentsUpdate[i].concat("; " + facultyCommentsNew[i]);
                    } else if (!facultyCommentsNew[i].equals("")) {
                        facultyCommentsUpdate[i] = facultyCommentsUpdate[i].concat(facultyCommentsNew[i]);
                    }
                }
            }
        }
        try {
            query = "SELECT responseId from responses where labId = " + labId + " AND compliant = 'No'";
            con = DatabaseConnection.getConnection();
            ps = con.prepareStatement(query);
            responseRS = ps.executeQuery();
            if (skip == 0) {
                query = "UPDATE responses set facultyComments = case ";
                String responseIdsUsed = "( ";
                int counter = 0;
                while (responseRS.next()) {
                    if (counter != 0) {
                        responseIdsUsed = responseIdsUsed.concat(", ");
                    }
                    query = query.concat("\nwhen responseId = " + responseRS.getInt(1) + " then '" + facultyCommentsUpdate[counter] + "'");

                    responseIdsUsed = responseIdsUsed.concat(responseRS.getInt(1) + "");
                    counter++;

                }
                responseIdsUsed = responseIdsUsed.concat(")");
                query = query.concat("\n end where responseId in " + responseIdsUsed);

                ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(DataCRUD.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (con != null) {
                con.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (responseRS != null) {
                responseRS.close();
            }

        }

        DataCRUD dc = new DataCRUD();

        request.setAttribute("userName", userName);
        request.setAttribute("fullName", fullName);
        request.setAttribute("AdminViewSwitch", viewSwitch);
        String fullNameFormat = fullNameFormat(fullName);
        ArrayList<Integer> labIdsArraylist = dc.getLabIdsFromFullNameUserView(fullNameFormat);
        ArrayList<LabAudit> labAuditsArraylist = new ArrayList<>();
        for (Integer labIdsArraylist1 : labIdsArraylist) {
            labAuditsArraylist.add(DataCRUD.getLabDetailsFromLabIdUserView(labIdsArraylist1));
        }
        request.setAttribute("LabAuditsArray", labAuditsArraylist);

        RequestDispatcher rd = request.getRequestDispatcher("UserLabs.jsp");

        rd.forward(request, response);
    }

    public static String fullNameFormat(String fullName) {

        if (!fullName.contains(",")) {
            String[] fullNameSplit = fullName.split(" ");
            String firstName = fullNameSplit[0];
            String lastName = fullNameSplit[1];

            String fullNameFormatted = lastName + ", " + firstName;
            return fullNameFormatted;
        }
        return fullName;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FacultyCorrectiveActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FacultyCorrectiveActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
