package labInspection;


import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Lab Audit Servlet", urlPatterns = {"/labAuditServlet"})
public class LabAuditServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {      
        
            java.util.Date Date = new java.util.Date();
            
            HttpSession sess = request.getSession();
            
            String roomNumber = request.getParameter("roomNumber");
            request.setAttribute("roomNumber", roomNumber);
            String building = request.getParameter("building");
            request.setAttribute("building", building);
            String discipline = request.getParameter("discipline");
            request.setAttribute("discipline", discipline);
            String responsibleIndividual = request.getParameter("responsiblePerson");
            request.setAttribute("responsibleIndividual", responsibleIndividual);
            String roomAssignment = request.getParameter("roomNumber");
            request.setAttribute("roomAssignment", roomAssignment);
            String status = "Waiting to Send";
            Date inspectionDate = Date;
            
            
            LabAudit labAudit = new LabAudit(building, roomNumber, discipline, responsibleIndividual, roomAssignment, inspectionDate, status);
            
            DataCRUD dc = new DataCRUD();
            //insert Lab details and fetch lab ID
            int labId = dc.addNewLab(labAudit);
            request.setAttribute("labId", labId);
            
            String[] categories = request.getParameterValues("categories");
            String categoryID = "";
                        for(String c : categories) {
                categoryID += c + ","; 
            }
            categoryID = categoryID.substring(0, categoryID.length()-1);
            sess.setAttribute("category",categoryID);
            
            request.setAttribute("Questions", DataCRUD.getQuestions(categoryID));
            RequestDispatcher rd = request.getRequestDispatcher("Questions.jsp");
            rd.forward(request, response);


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "This servlet displays the current date and time.";
    }// </editor-fold>

}
