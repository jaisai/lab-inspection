/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

/**
 *
 * @author s515739
 */
public class ExportQuestion {
    
    private int labsInspected;
    private int reportsReturned;
    private int compliant;
    private int nonCompliant;
    private int na;
    private int complianceRate;

    public ExportQuestion(int labsInspected, int reportsReturned, int compliant, int nonCompliant, int na) {
        this.labsInspected = labsInspected;
        this.reportsReturned = reportsReturned;
        this.compliant = compliant;
        this.nonCompliant = nonCompliant;
        this.na = na;
        complianceRate = ((this.compliant - this.nonCompliant)/this.compliant) * 100;
    }

    public int getLabsInspected() {
        return labsInspected;
    }

    public void setLabsInspected(int labsInspected) {
        this.labsInspected = labsInspected;
    }

    public int getReportsReturned() {
        return reportsReturned;
    }

    public void setReportsReturned(int reportsReturned) {
        this.reportsReturned = reportsReturned;
    }

    public int getCompliant() {
        return compliant;
    }

    public void setCompliant(int compliant) {
        this.compliant = compliant;
    }

    public int getNonCompliant() {
        return nonCompliant;
    }

    public void setNonCompliant(int nonCompliant) {
        this.nonCompliant = nonCompliant;
    }

    public int getNa() {
        return na;
    }

    public void setNa(int na) {
        this.na = na;
    }

    public int getComplianceRate() {
        return complianceRate;
    }

    public void setComplianceRate(int complianceRate) {
        this.complianceRate = complianceRate;
    }
    
    
    
}
