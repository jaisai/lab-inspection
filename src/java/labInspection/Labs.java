/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

/**
 *
 * @author s515739
 */
public class Labs {
    private int labInfoId;
    private String building;
    private String discipline;
    private String roomNumber;
    private String roomAssignment;
    private String responsibleIndividual;

    public Labs(int labInfoId, String building, String discipline, String roomNumber, String roomAssignment, String responsibleIndividual) {
        this.labInfoId = labInfoId;
        this.building = building;
        this.discipline = discipline;
        this.roomNumber = roomNumber;
        this.roomAssignment = roomAssignment;
        this.responsibleIndividual = responsibleIndividual;
    }

    public int getLabInfoId() {
        return labInfoId;
    }

    public void setLabInfoId(int labInfoId) {
        this.labInfoId = labInfoId;
    }
    

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomAssignment() {
        return roomAssignment;
    }

    public void setRoomAssignment(String roomAssignment) {
        this.roomAssignment = roomAssignment;
    }

    public String getResponsibleIndividual() {
        return responsibleIndividual;
    }

    public void setResponsibleIndividual(String responsibleIndividual) {
        this.responsibleIndividual = responsibleIndividual;
    }
    
    
}
