package labInspection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author S525096
 */
public class Categories {
    private int categoriesId;
    private String categoriesType;

    public Categories(int categoriesId, String categoriesType) {
        this.categoriesId = categoriesId;
        this.categoriesType = categoriesType;
    }

    public int getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(int categoriesId) {
        this.categoriesId = categoriesId;
    }

    public String getCategoriesType() {
        return categoriesType;
    }

    public void setCategoriesType(String categoriesType) {
        this.categoriesType = categoriesType;
    }

}
