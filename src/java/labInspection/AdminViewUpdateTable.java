/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author s515739
 */
@WebServlet(name = "AdminViewUpdateTable", urlPatterns = {"/AdminViewUpdateTable"})
public class AdminViewUpdateTable extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String year = request.getParameter("year");
        
        List<LabAudit> inspectionLabsList = DataCRUD.getLabDetailsFromYear(Integer.parseInt(year));
        
        
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        
        for (LabAudit inspectionLabsList1 : inspectionLabsList) {
            obj.put("labId", inspectionLabsList1.getLabId());
            obj.put("building", inspectionLabsList1.getBuilding());
            obj.put("roomNumber", inspectionLabsList1.getRoomNumber());
            obj.put("discipline", inspectionLabsList1.getDiscipline());
            obj.put("responsibleIndividual", inspectionLabsList1.getResponsibleIndividualName());
            obj.put("roomAssignment", inspectionLabsList1.getRoomAssignment());
            obj.put("inspectionDate", inspectionLabsList1.getInspectionDate());
            obj.put("status", inspectionLabsList1.getStatus());
            obj.put("inspectionDateTable", inspectionLabsList1.getInspectionDateTable().getTime());
            
            array.add(obj);
            obj = new JSONObject();
        }
        
        
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
                out.println(array);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
