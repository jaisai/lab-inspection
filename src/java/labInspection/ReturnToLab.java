/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author s515739
 */
@WebServlet(name = "ReturnToLab", urlPatterns = {"/ReturnToLab"})
public class ReturnToLab extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String getQuestions = (String) request.getParameter("getQuestions"); //if this is null then the user is going to Questions, if it is true then the page is trying to get the questions for the lab

        if (getQuestions == null) {
            HttpSession sess = request.getSession();
            int labId = Integer.parseInt(request.getParameter("EditLabId"));
            LabAudit lab = DataCRUD.getLabDetailsFromLabId(labId);
            String query = "select categoryId from questions JOIN responses ON questions.questionId = responses.questionId WHERE labId = " + labId + " group by categoryId";
            ResultSet categoryIdRS = null;
            Connection con = null;
            PreparedStatement ps = null;
            String categoryId = "";
            try {
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement(query);
                categoryIdRS = ps.executeQuery();

                while (categoryIdRS.next()) {

                    categoryId += categoryIdRS.getInt(1) + ",";

                }
                categoryId = categoryId.substring(0, categoryId.length() - 1);
                categoryIdRS.close();

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(ReturnToLab.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                try {
                    if (con != null) {
                        con.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (categoryIdRS != null) {
                        categoryIdRS.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ReturnToLab.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            request.setAttribute("ReturnLab", "true");
            sess.setAttribute("category", categoryId);
            request.setAttribute("labId", labId);
            request.setAttribute("roomNumber", lab.getRoomNumber());
            request.setAttribute("building", lab.getBuilding());
            request.setAttribute("responsibleIndividual", lab.getResponsibleIndividualName());
            request.setAttribute("Questions", DataCRUD.getQuestions(categoryId));
            RequestDispatcher rd = request.getRequestDispatcher("Questions.jsp");
            rd.forward(request, response);
        } else {
            int labId = Integer.parseInt((String) request.getParameter("labId"));
            LabAudit lab = DataCRUD.getLabDetailsFromLabIdUserView(labId);
            ArrayList<Responses> responses = DataCRUD.getResponsesFromLabId(labId);

            JSONArray array = new JSONArray();
            JSONObject obj = new JSONObject();

            for (Responses resp : responses) {

                obj.put("compliant", resp.getCompliant());
                obj.put("questionId", resp.getQuestionId());
                if (resp.getCompliant().equals("No")) {
                    obj.put("comment", resp.getComments());
                }
                array.add(obj);
                obj = new JSONObject();
            }
            request.setAttribute("roomNumber", lab.getRoomNumber());
            request.setAttribute("building", lab.getBuilding());
            request.setAttribute("responsibleIndividual", lab.getResponsibleIndividualName());
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {

                out.println(array);

            }

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
