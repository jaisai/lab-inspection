/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S525096
 */
@WebServlet(name = "RoomAssignmentServlet", urlPatterns = {"/RoomAssignmentServlet"})
public class RoomAssignmentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        //gets potentially sent paramenters (some may be null)
        String building = request.getParameter("building");
        String discipline = request.getParameter("discipline");
        String selectedRoomAssignment = request.getParameter("selectRoomAssignment");

        ArrayList<String> buildingList = new ArrayList<>();
        ArrayList<String> disciplineList = new ArrayList<>();
        ArrayList<String> assignmentList = new ArrayList<>();
        ArrayList<String> roomNumAndPerson = new ArrayList<>();

        RoomAssignment rooms = new RoomAssignment();
        if (building == null) {
            buildingList = rooms.getBuildings();
        } else if (discipline == null) {
            disciplineList = rooms.getDiscipline(building);
        } else if (selectedRoomAssignment == null) {
            assignmentList = rooms.getNumbers(building, discipline);
        }else {
            roomNumAndPerson.add(rooms.getAssignment(selectedRoomAssignment));
            roomNumAndPerson.add(rooms.getResponsiblePerson(selectedRoomAssignment));
        }
        rooms.closeRS();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            assignmentList.stream().forEach((assignment) -> {
                out.println(assignment);
            });

            buildingList.stream().forEach((build) -> {
                out.println(build);
            });

            disciplineList.stream().forEach((disc) -> {
                out.println(disc);
            });
            roomNumAndPerson.stream().forEach((roomNPers) -> {
                out.println(roomNPers);
            });
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(RoomAssignmentServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(RoomAssignmentServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
