/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labInspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S525051
 */
@WebServlet(name = "FacultyCAViewNavigation", urlPatterns = {"/FacultyCAViewNavigation"})
public class FacultyCAViewNavigation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String test = request.getParameter("CorrectiveActionsLabId");

        int id = Integer.parseInt(request.getParameter("CorrectiveActionsLabId"));
        LabAudit labAudit = DataCRUD.getLabDetailsFromLabId(id);
        ArrayList<Responses> responses;
        responses = DataCRUD.getResponsesFromLabId(id);
        ArrayList<String> question = new ArrayList<>();
        int Done = 1;
        for (Responses csmresponse : responses) {
            if (csmresponse.getCompliant().equals("No")) {
                Done = 0;
            }
            question.add(csmresponse.getQuestion());
        }
        String building = labAudit.getBuilding();
        String discipline = labAudit.getDiscipline();
        String responsibleIndividualName = labAudit.getResponsibleIndividualName();
        String status = labAudit.getStatus();
        String inspectionDate = labAudit.getInspectionDate();
        String roomAssignment = labAudit.getRoomAssignment();
        String roomNumber = labAudit.getRoomNumber();

        try (PrintWriter out = response.getWriter()) {

            request.setAttribute("correctiveActionsLabId", id);
            request.setAttribute("Building", building);

            request.setAttribute("Discipline", discipline);
            request.setAttribute("ResponsibleIndividualName", responsibleIndividualName);
            request.setAttribute("Status", status);
            request.setAttribute("InspectionDate", inspectionDate);
            request.setAttribute("RoomAssignment", roomAssignment);
            request.setAttribute("RoomNumber", roomNumber);
            request.setAttribute("CSMResponses", responses);
            request.setAttribute("Completed", Done);
            RequestDispatcher rd = request.getRequestDispatcher("UserView.jsp");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
