<%-- 
    Document   : questions
    Created on : Oct 4, 2016, 2:37:36 PM
    Author     : S525096
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
            function includeComments(questionId, value)
            {

                var comment = document.getElementById(questionId + "comments");
                if (value === "No")
                {
                    comment.hidden = "";
                    comment.required = true;
                }
                else
                {
                    comment.hidden = "hidden";
                }
            }
            function saveLab() {

                document.Questions.action = "SaveLabServlet";
                document.Questions.submit();

            }


            function getQuestions() {
                var check = '${ReturnLab}';

                if (check == 'true') {

                    var labId = document.getElementById("labId");

                    requestTable = new XMLHttpRequest();
                    var url = "ReturnToLab?getQuestions=true&labId=" + labId.value;
                    requestTable.open("POST", url, true);
                    requestTable.onreadystatechange = displayQuestions;
                    requestTable.send(null);

                }

            }

            function displayQuestions() {
                //readyState 4 = request has finished and ready 
                //staus 200 = OK (a status of 404 = page not found)
                if (requestTable.readyState === 4 && requestTable.status === 200) {

                    var JSONObj = JSON.parse(requestTable.responseText);

                    for (var i = 0; i < JSONObj.length; i++) {


                        if (JSONObj[i].compliant === "Yes") {
                            var yes = document.getElementById(JSONObj[i].questionId + "Yes");
                            yes.checked = true;
                        } else if (JSONObj[i].compliant === "No") {
                            var no = document.getElementById(JSONObj[i].questionId + "No");
                            no.checked = true;

                            var comment = document.getElementById(JSONObj[i].questionId + "comments");
                            comment.value = JSONObj[i].comment;

                            comment.hidden = "";
                            
                        }
                    }
                }
            }

            window.onload = function () {

                getQuestions();
            }

        </script>
    </head>
    <script>
        function goBack() {
            window.history.back();
        }

    </script>

    <div class="background"></div>

    <body class="bodyQuestion">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <ul class ="nav navbar-nav">
                    <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </button>

                    <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

                </ul>
                <% String fullName = (String) session.getAttribute("fullName");%>
                <span class ="navbar-text">Signed in as <%= fullName%></span>
                <% session.setAttribute("fullName", fullName);%>



                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
        </nav>
        <div style="width: 1200px; margin-left: auto; margin-right: auto;">
            <form name="Questions" action="QuestionsServlet" method="post">
                <%
                    int inspectionLab = (Integer) request.getAttribute("labId");
                    String roomNumber = (String) request.getAttribute("roomNumber");
                    String responsibleIndividual = (String) request.getAttribute("responsibleIndividual");
                    
                %>                
                <input type="hidden" name="LabDetails" value="<%=inspectionLab%>"/>
                <div align="center" class="form" style=" border:2px solid #258e12; border-top-left-radius: 15px; border-top-right-radius: 15px; width: 1200px; background: #ffffff">                   
                    <table class="table" style="table-layout: fixed">
                        <tr>
                            <td align="center"><h4 style="color: black">Responsible Individual</h4><%=responsibleIndividual%></td>  
                            <td align="center"><h4 style="color: black">Room No</h4><%=roomNumber%></td>
                            <td align="center"><h4 style="color: black">Lab ID</h4><%=inspectionLab%></td>
                            <td align="center"><h4 style="color: black">Building</h4>${building}</td>
                            <input hidden="hidden" value="${building}" name="building">
                            <input hidden="hidden" value="<%=roomNumber%>" name="roomNumber">
                        </tr>
                    </table>
                </div>
                <div align="center" class="form" style=" border:2px solid #258e12; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;  width: 1200px; background: #ffffff; overflow-x: auto">
                    <c:set var="lastCategory" scope="session" value="" />

                    <style>
                        .noborder
                        {
                            border-style:hidden;
                            border-right-style: hidden;
                        }

                    </style>
                    <table class="table" style="table-layout:fixed; width:100%">
                        <thead>
                            <tr>
                                <th>Question</th>
                                <th><label>Comments</label></th>
                                <th>Compliant?</th>
                            </tr>
                        </thead>

                        <tbody id="questionTableBody">

                            <c:forEach items="${Questions}" var="p">
                                <c:set var="currentCategory" scope="session" value="${p.categoryName}"/>

                                <c:if test="${currentCategory != lastCategory || lastCategory == ''}">
                                    <tr> 
                                        <td><b>${p.categoryName}</b></td>
                                    <tr>
                                    </c:if>
                                <tr  >
                                    <td name="${p.questionId}question">${p.question}</td>
                                    <td border="none" id="${p.questionId}" name="${p.questionId}comments">
                                        <textarea hidden="hidden" rows="1" cols="50" id="${p.questionId}comments" name="${p.questionId}comments"></textarea>  
                                    </td>
                                    <td><div class="radio">
                                            <label><input type="radio" value="Yes" required name="${p.questionId}" id="${p.questionId}Yes" onclick="includeComments(${p.questionId}, 'Yes')">Yes</label>
                                            <label><input type="radio" value="No" name="${p.questionId}" id="${p.questionId}No" onclick="includeComments(${p.questionId}, 'No')">No</label>
                                        </div></td>
                                </tr>

                                <c:set var="lastCategory" scope="session" value="${currentCategory}" />
                            </c:forEach>



                        </tbody>

                    </table>
                    

                    <button style="margin-left:30%; margin-bottom: 3%; width:15%; text-align: center;" type="button" class="btn btn-primary pull-left" onclick="saveLab()">Save Lab</button>
                    <input hidden="hidden" id="labId" name="labId" value="${labId}">
                    <button style="margin-right:30%; margin-bottom: 3%; width:15%; text-align: center;" type="submit" class="btn btn-primary pull-right">Finish Inspection</button>
                    
                </div>

            </form>

        </div>
    </body>

</html>
