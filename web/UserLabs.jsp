

<%-- 
    Document   : UserLabs
    Created on : Nov 21, 2016, 3:07:29 PM
    Author     : S525096
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="labInspection.DatabaseConnection"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Lab Inspection Audit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">       
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="css/labinspection.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="ejs_production.js"></script>

    <script>

        function assign(id)
        {
            var edittextbox = document.getElementById("CorrectiveActionsLabId");
            edittextbox.value = "" + id;
            document.FacultyCorectiveActionsForm.action = "FacultyCAViewNavigation";
            document.FacultyCorectiveActionsForm.submit();
        }
        function generateReport(id)
        {
            var edittextbox = document.getElementById("CorrectiveActionsLabId");
            edittextbox.value = "" + id;
            document.FacultyCorectiveActionsForm.action = "CSMReportServlet";
            document.FacultyCorectiveActionsForm.submit();
        }

        function goBack() {
            window.history.back();
        }



    </script>

</head>


<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class ="nav navbar-nav">
                <button type="button" class="btn btn-default navbar-btn btn-sm" onclick="goBack()" aria-label="Left Align">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </button>

                <img src="css/NorthwestIcon.jpg" alt="" height="30"/> 

            </ul>

            <% String fullName = (String) request.getAttribute("fullName");%>
            <span class ="navbar-text">Signed in as <%= fullName%></span>
            <% session.setAttribute("fullName", fullName);%>
            <%
                String userName = (String) request.getAttribute("userName");
                session.setAttribute("userName", userName);%>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" onclick="window.location.href = 'Login.jsp'"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </nav>


    <%
        String adminViewSwitch = (String) request.getAttribute("AdminViewSwitch");
        if (adminViewSwitch == null) {
            adminViewSwitch = (String) session.getAttribute("viewSwitch");
        }
        session.setAttribute("viewSwitch", adminViewSwitch);
        if (adminViewSwitch.equals("true")) {%>
    <div class="col-md-3">
        <form action="AdminUserSwitchServlet" method="post">
            <input hidden="hidden" name="fromAdminView" value="0">
            <input hidden="hidden" name="userName" value="<%=userName%>">
            <input hidden="hidden" name="viewSwitch" value="<%=adminViewSwitch%>">
            <button class="btn btn-primary" type="submit">Switch to Admin View</button>
        </form>
    </div>
    <%}%>
    <form align="center" name="FacultyCorectiveActionsForm" action="">
        <div align="center" class="form">
            <div class="form" style="max-width: 1200px; margin-left: auto; margin-right: auto;">

                <br><br>
                <div class="form" align="center" style=" border:2px solid #258e12; border-radius: 25px; background: #ffffff"> 

                    <table class="table">
                        <thead >
                            <tr >
                                <th>Building</th>
                                <th>Room Number</th>
                                <th>Inspection Date</th>
                                <th>Faculty Inspection</th>
                            </tr>

                        </thead>

                        <tbody>
                            <c:forEach var="labAudit" items="${LabAuditsArray}">
                                <tr>
                                    <td>
                                        ${labAudit.building}
                                    </td>
                                    <td>
                                        ${labAudit.roomNumber}
                                    </td>
                                    <td>
                                        ${labAudit.formattedInspectionDate}       
                                    </td>

                                    <td>
                                        <button class="btn btn-primary btn-sm" type="Submit" onclick="assign(${labAudit.labId})" name="${labAudit.labId}edit" id="${labAudit.labId}changes" style="background-color:#49743D;font-weight:bold;color:#ffffff;">Corrective Actions Made</button>
                                    </td>


                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                    <input type="hidden" name="CorrectiveActionsLabId" value="" id="CorrectiveActionsLabId"/>
                </div>
            </div>
        </div>
    </form>
</body>



